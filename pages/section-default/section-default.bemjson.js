({
    block: 'b-page',
    'x-ua-compatible': 'IE=edge,chrome=1',
    title: 'Список товаров',
    head: [
        { elem: 'js', url: '../../js/jquery-1.10.0.min.js'},
        { elem: 'css', url: 'section-default.css'},
        { elem: 'css', url: 'section-default.ie.css', ie: 'ie'}
    ],
    content: [
        {
            block: 'b-page-inner',
            content: [
                {
                    block: 'b-header'
                },
                {
                    block: 'b-content',
                    content: [
                        {
                            elem: 'row',
                            elemMods: {color: 'menu'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-great-menu',
                                    list: [
                                        {
                                            title: 'Каталог',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Автосигнализации',
                                            url: '#',
                                            type: 'active',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Предпусковые подогреватели',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'hub'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-hub',
                                    list: [
                                        {
                                            url: '#',
                                            title: 'Автосигнализации'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры',
                                            type: 'active'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Web'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        },
                                        {
                                            url: '#',
                                            title: 'Авто'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Webasto'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            content: {
                                block: 'b-container',
                                content: [
                                    {
                                        block: 'b-pathway',
                                        list: [
                                            {
                                                url: '#',
                                                title: 'Каталог товаров'
                                            },
                                            {
                                                url: '#',
                                                title: 'Противоугонные устройства'
                                            }
                                        ]
                                    },
                                    {
                                        block: 'b-products',
                                        cls: 'i-clearfix',
                                        content: [
                                            {
                                                elem: 'filter',
                                                content: {
                                                    block: 'b-filter',
                                                    list: [
                                                        {
                                                            title: 'Цена, руб',
                                                            content: {
                                                                block: 'b-range',
                                                                min: 0,
                                                                max: 12000
                                                            }
                                                        },
                                                        {
                                                            title: 'Производители',
                                                            type: 'brand',
                                                            content: [
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'StarLine'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Tomahawk'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Alligator'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Scher-Khan'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Jaguar'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Pandora'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'KGB',
                                                                    checked: 'checked',
                                                                    cls: 'checked'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Pantera'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Legendford'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Cenmax'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            title: 'Тип',
                                                            type: 'switcher-on',
                                                            content: [
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Автосигнализация с автозапуском'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Автосигнализация без автозапуска',
                                                                    checked: 'checked',
                                                                    cls: 'checked'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            title: 'Радиус действия, м',
                                                            type: 'switcher-on',
                                                            content: {
                                                                block: 'b-range',
                                                                min: 0,
                                                                max: 5000
                                                            }
                                                        },
                                                        {
                                                            title: 'Мощность передатчика, мВт',
                                                            type: 'switcher-off',
                                                            content: {
                                                                block: 'b-range',
                                                                min: 0,
                                                                max: 500
                                                            }
                                                        },
                                                        {
                                                            title: 'Противоугонные функции',
                                                            type: 'switcher-off',
                                                            content: [
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Автосигнализация с автозапуском'
                                                                },
                                                                {
                                                                    block: 'b-form-control',
                                                                    mods: {type: 'checkbox'},
                                                                    mix: {block: 'b-filter', elem: 'checkbox'},
                                                                    label: 'Автосигнализация без автозапуска',
                                                                    checked: 'checked',
                                                                    cls: 'checked'
                                                                }
                                                            ]
                                                        },
                                                        {
                                                            content: [
                                                                {
                                                                    block: 'b-form-control',
                                                                    elem: 'input',
                                                                    mix: {block: 'b-filter', elem: 'button'},
                                                                    cls: 'i-inline',
                                                                    attrs: {
                                                                        value: 'Подобрать',
                                                                        type: 'submit'
                                                                    }
                                                                },
                                                                {
                                                                    block: 'b-link',
                                                                    mix: {block: 'b-filter', elem: 'reset'},
                                                                    cls: 'i-inline',
                                                                    url: '#',
                                                                    content: 'Сброcить'
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            },
                                            {
                                                elem: 'main',
                                                content: [
                                                    {
                                                        elem: 'title',
                                                        tag: 'h1',
                                                        content: 'Автосигнализации'
                                                    },
                                                    {
                                                        block: 'b-product-link',
                                                        list: [
                                                            {
                                                                url: '#',
                                                                title: 'Дешевые сигнализации'
                                                            },
                                                            {
                                                                url: '#',
                                                                title: 'Сигнализации с обратной связью'
                                                            },
                                                            {
                                                                url: '#',
                                                                title: 'Автосигнализации с автозапуском и обратной связь'
                                                            },
                                                            {
                                                                url: '#',
                                                                title: 'С обратной связлью'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        elem: 'top',
                                                        content: [
                                                            {
                                                                block: 'b-pagination',
                                                                mods: {position: 'right'},
                                                                current: 1,
                                                                finish: 12,
                                                                pages: [1,2,3,4,5]
                                                            },
                                                            {
                                                                elem: 'toggle-block',
                                                                cls: 'i-inline',
                                                                content: [
                                                                    {
                                                                        elem: 'toggle-text',
                                                                        elemMods: {state: 'active'},
                                                                        cls: 'i-inline clickme',
                                                                        content: 'По названию'
                                                                    },
                                                                    {
                                                                        elem: 'toggle',
                                                                        mix: {block: 'i-inline'},
                                                                        cls: 'light',
                                                                        attrs: {
                                                                            'data-checkbox': 'checkme',
                                                                            'rel': 'clickme'
                                                                        }
                                                                    },
                                                                    {
                                                                        block: 'b-form-control',
                                                                        elem: 'input',
                                                                        cls: 'checkme',
                                                                        attrs: {
                                                                            type: 'checkbox',
                                                                            disabled: 'disabled'
                                                                        }
                                                                    },
                                                                    {
                                                                        elem: 'toggle-text',
                                                                        cls: 'i-inline clickme',
                                                                        content: 'По цене'
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        elem: 'body',
                                                        content: [
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация StarLine A91',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product1.png',
                                                                price: '2800'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                mods: {type: 'new'},
                                                                url: '#',
                                                                title: 'Автосигнализация StarLine B9/A91',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product2.png',
                                                                price: '2800',
                                                                oldprice: '3800'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product3.png',
                                                                price: '6490'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                mods: {type: 'sale'},
                                                                url: '#',
                                                                title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product4.png',
                                                                price: '2990',
                                                                oldprice: '3990'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация StarLine A91',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product1.png',
                                                                price: '2800'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация StarLine B9/A91',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product2.png',
                                                                price: '2800',
                                                                oldprice: '3800'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация Scher-Khan Magicar 5',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product3.png',
                                                                price: '6490'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product4.png',
                                                                price: '2990',
                                                                oldprice: '3990'
                                                            },
                                                            {
                                                                block: 'b-product',
                                                                url: '#',
                                                                title: 'Автосигнализация StarLine A91',
                                                                category: 'Автосигнализации',
                                                                categorylink: '#',
                                                                brand: '../../images/temp/brand.png',
                                                                image: '../../images/temp/product1.png',
                                                                price: '2800'
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        elem: 'bottom',
                                                        content: {
                                                            block: 'b-pagination',
                                                            current: 1,
                                                            finish: 12,
                                                            pages: [1,2,3,4,5]
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'grey'},
                            content: {
                                block: 'b-container',
                                cls: 'i-clearfix',
                                content: [
                                    {
                                        block: 'b-brand',
                                        url: '#',
                                        count: '57',
                                        list: [
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand1.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand2.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand3.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand4.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand5.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand6.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand7.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand8.png'
                                            }
                                        ]
                                    },
                                    {
                                        block: 'b-seo',
                                        title: 'Интернет-магазин противоугонных систем',
                                        content: '<p>Автосигнализация — электронное устройство, установленное в автомобиль, предназначенное для его защиты от угона, кражи компонентов данного <a href="#">транспортного средства</a> или вещей, находящихся в автомобиле. Оно оповещает владельца о несанкционированном доступе к автомобилю (салону), но не препятствует угону, краже автокомпонентов и т.п.</p><p>Диалоговый код — специальный способ кодозащищённости автосигнализаций. Использует для идентификации брелка широко известную в криптографии технологию аутентификации через незащищённый канал. Получив сигнал, система убеждается, что он послан со «своего» брелка.</p>'
                                    }
                                ]
                            }
                        },
                        {
                            elem: 'row',
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-menu',
                                    mods: {type: 'horiz', position: 'hub'},
                                    list: [
                                        {
                                            url: '#',
                                            title: 'Автосигнализации'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Webasto'
                                        },
                                        {
                                            url: '#',
                                            title: 'Коаксиальная автоакустика'
                                        },
                                        {
                                            url: '#',
                                            title: 'Потолочные мониторы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'Компонентная автоакустика'
                                        },
                                        {
                                            url: '#',
                                            title: 'Встраиваемые мониторы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        },
                                        {
                                            url: '#',
                                            title: 'Штатная автоакустика'
                                        },
                                        {
                                            url: '#',
                                            title: 'Подголовники с монитором'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автосирены'
                                        },
                                        {
                                            url: '#',
                                            title: 'Компоненты автоакустики'
                                        },
                                        {
                                            url: '#',
                                            title: 'Салонные зеркала'
                                        }
                                    ]
                                }
                            }
                        }
                    ]
                },
                {
                    elem: 'footer'
                }
            ]
        },
        {
            block: 'b-footer'
        },
        {
            block: 'b-scripts',
            content: [
                { elem: 'js', url:'../merged/_merged.js'}
            ]
        }
    ]
})
