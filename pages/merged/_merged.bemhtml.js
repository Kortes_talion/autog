var BEMHTML = function() {
  var cache,
      xjst = (function(exports) {
    function $1(__$ctx) {
        var __t = __$ctx._mode;
        if (__t === "content") {
            return $2(__$ctx);
        } else if (__t === "js") {
            return $219(__$ctx);
        } else if (__t === "attrs") {
            return $307(__$ctx);
        } else if (__t === "tag") {
            return $360(__$ctx);
        } else if (__t === "mix") {
            return $444(__$ctx);
        } else if (__t === "cls") {
            return $181(__$ctx);
        } else if (__t === "mods") {
            if (__$ctx.block === "b-debug-toolbar") {
                if (!!__$ctx.elem === false) {
                    return {
                        state: "show"
                    };
                    return;
                } else {
                    return $536(__$ctx);
                }
            } else {
                return $536(__$ctx);
            }
        } else if (__t === "default") {
            return $493(__$ctx);
        } else if (__t === "xUACompatible") {
            if (__$ctx.block === "b-page") {
                if (!!__$ctx.elem === false) {
                    return $521(__$ctx);
                } else {
                    return $536(__$ctx);
                }
            } else {
                return $536(__$ctx);
            }
        } else if (__t === "doctype") {
            if (__$ctx.block === "b-page") {
                if (!!__$ctx.elem === false) {
                    return $529(__$ctx);
                } else {
                    return $536(__$ctx);
                }
            } else {
                return $536(__$ctx);
            }
        } else if (__t === "jsAttr") {
            return $535(__$ctx);
        } else if (__t === "bem") {
            return $457(__$ctx);
        } else {
            return $536(__$ctx);
        }
    }
    function $2(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-catalog") {
            if (!!__$ctx.elem === false) {
                return $5(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-brand") {
            if (!!__$ctx.elem === false) {
                return $83(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-info") {
            if (!!__$ctx.elem === false) {
                return $88(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-product") {
            if (!!__$ctx.elem === false) {
                return $93(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-product-list") {
            if (!!__$ctx.elem === false) {
                return $98(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-seo") {
            if (!!__$ctx.elem === false) {
                return [ {
                    elem: "title",
                    tag: "h1",
                    content: __$ctx.ctx.title
                }, {
                    elem: "body",
                    content: __$ctx.ctx.content
                } ];
                return;
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-order-form") {
            if (!!__$ctx.elem === false) {
                return $108(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-debug-toolbar") {
            if (__$ctx.elem === "action") {
                return {
                    content: __$ctx.ctx.content
                };
                return;
            } else {
                if (!!__$ctx.elem === false) {
                    var _$2ccontent = {
                        elem: "actions",
                        tag: "ul",
                        content: [ {
                            elem: "action",
                            action: "showFormError",
                            content: "FORMS: Error"
                        }, {
                            elem: "action",
                            action: "showFormSuccess",
                            content: "FORMS: Success"
                        } ]
                    };
                    return [ {
                        elem: "open"
                    }, {
                        elem: "panel",
                        content: _$2ccontent
                    } ];
                    return;
                } else {
                    return $180(__$ctx);
                }
            }
        } else if (__t === "b-order-item") {
            if (!!__$ctx.elem === false) {
                return $121(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-order-count") {
            if (!!__$ctx.elem === false) {
                return $126(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-order") {
            if (!!__$ctx.elem === false) {
                var _$21ctx = [];
                _$21ctx.push([ {
                    elem: "main",
                    content: __$ctx.ctx.content
                }, {
                    elem: "bottom",
                    content: [ {
                        elem: "all",
                        content: "Итого стоимость заказа"
                    }, {
                        elem: "price",
                        cls: "i-inline",
                        content: __$ctx.ctx.price
                    }, {
                        block: "b-link",
                        mix: {
                            block: "b-order",
                            elem: "next"
                        },
                        cls: "i-inline",
                        url: "#",
                        content: "Оформить заказ"
                    } ]
                } ]);
                return _$21ctx;
                return;
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-gallery") {
            var __t = __$ctx.elem;
            if (__t === "item") {
                return $71(__$ctx);
            } else if (__t === "image") {
                var _$2vcontent = [ {
                    block: "b-link",
                    mix: {
                        block: "b-gallery",
                        elem: "image-link"
                    },
                    url: "#",
                    mods: {
                        pseudo: true
                    },
                    attrs: {
                        "data-image-full": __$ctx.ctx.full
                    },
                    content: {
                        block: "b-icon",
                        mix: {
                            block: "b-gallery",
                            elem: "img"
                        },
                        url: __$ctx.ctx.image
                    }
                } ];
                return _$2vcontent;
                return;
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-pathway") {
            if (!!__$ctx.elem === false) {
                return $136(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-related-product") {
            if (!!__$ctx.elem === false) {
                return $66(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-hub") {
            if (!!__$ctx.elem === false) {
                return $141(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-card") {
            if (!!__$ctx.elem === false) {
                return $61(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-great-menu") {
            if (!!__$ctx.elem === false) {
                return $146(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-category") {
            if (!!__$ctx.elem === false) {
                return $56(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-header") {
            if (!!__$ctx.elem === false) {
                return $151(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-pagination") {
            if (!!__$ctx.elem === false) {
                return $51(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-feedback") {
            if (!!__$ctx.elem === false) {
                var _$1sctx = [];
                _$1sctx.push({
                    elem: "inner",
                    content: [ "Есть вопрос?", {
                        block: "b-link",
                        mix: {
                            block: "b-feedback",
                            elem: "link"
                        },
                        url: __$ctx.ctx.url,
                        content: {
                            elem: "text",
                            cls: "i-inline",
                            content: "Напишите нам"
                        }
                    } ]
                });
                return _$1sctx;
                return;
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-form-control") {
            if (!(__$ctx.mods && __$ctx.mods.type === "checkbox") === false) {
                if (!!__$ctx.elem === false) {
                    var _$38ctx = [];
                    var _$38input_content = [];
                    _$38input_content.push({
                        elem: "input",
                        attrs: {
                            checked: __$ctx.ctx.checked,
                            name: __$ctx.ctx.name ? __$ctx.ctx.name : __$ctx.generateId(),
                            type: "checkbox"
                        }
                    });
                    if (__$ctx.ctx.label) {
                        _$38ctx.push({
                            elem: "label",
                            elemMods: {
                                from: "checkbox"
                            },
                            content: [ _$38input_content, __$ctx.ctx.label ]
                        });
                    } else {
                        _$38ctx.push(_$38input_content);
                    }
                    return _$38ctx;
                    return;
                } else {
                    return $29(__$ctx);
                }
            } else {
                return $29(__$ctx);
            }
        } else if (__t === "b-basket") {
            if (!!__$ctx.elem === false) {
                return $161(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-range") {
            if (!!__$ctx.elem === false) {
                return $20(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-search-form") {
            if (!!__$ctx.elem === false) {
                return $166(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-filter") {
            if (!!__$ctx.elem === false) {
                return $15(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-menu") {
            if (!!__$ctx.elem === false) {
                return $171(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-product-link") {
            if (!!__$ctx.elem === false) {
                var _$3hctx = [];
                var _$3hlist_content = [];
                var _$3hlist = __$ctx.ctx.list;
                for (var _$3hi = 0; _$3hi < _$3hlist.length; _$3hi++) {
                    _$3hlist_content.push({
                        elem: "item",
                        tag: "li",
                        cls: "i-inline",
                        content: {
                            block: "b-link",
                            mix: {
                                block: "b-product-link",
                                elem: "link"
                            },
                            url: _$3hlist[_$3hi].url,
                            content: _$3hlist[_$3hi].title
                        }
                    });
                }
                _$3hctx.push({
                    elem: "list",
                    tag: "ul",
                    content: _$3hlist_content
                });
                return _$3hctx;
                return;
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "i-ua") {
            if (!!__$ctx.elem === false) {
                return [ ";(function(d,e,c,r){", "e=d.documentElement;", 'c="className";', 'r="replace";', 'e[c]=e[c][r]("i-ua_js_no","i-ua_js_yes");', 'if(d.compatMode!="CSS1Compat")', 'e[c]=e[c][r]("i-ua_css_standart","i-ua_css_quirks")', "})(document);" ].join("");
                return;
            } else {
                return $180(__$ctx);
            }
        } else if (__t === "b-footer") {
            if (!!__$ctx.elem === false) {
                return $103(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else {
            return $180(__$ctx);
        }
    }
    function $5(__$ctx) {
        var _$3jctx = [];
        var _$3jlist_content = [];
        if (__$ctx.ctx.list) {
            var _$3jlist = __$ctx.ctx.list;
            for (var _$3ji = 0; _$3ji < _$3jlist.length; _$3ji++) {
                _$3jlist_content.push({
                    elem: "item",
                    tag: "li",
                    cls: "i-inline",
                    content: {
                        block: "b-link",
                        mix: {
                            block: "b-catalog",
                            elem: "link"
                        },
                        url: _$3jlist[_$3ji].url,
                        content: _$3jlist[_$3ji].title
                    }
                });
            }
        } else {
            undefined;
        }
        _$3jctx.push([ {
            block: "b-link",
            mix: {
                block: "b-catalog",
                elem: "image"
            },
            url: __$ctx.ctx.url,
            content: {
                block: "b-icon",
                mix: {
                    block: "b-catalog",
                    elem: "img"
                },
                url: __$ctx.ctx.image,
                alt: __$ctx.ctx.title
            }
        }, {
            elem: "body",
            content: [ {
                elem: "top",
                content: [ {
                    block: "b-link",
                    mix: {
                        block: "b-catalog",
                        elem: "title"
                    },
                    url: __$ctx.ctx.url,
                    content: __$ctx.ctx.title
                }, {
                    elem: "count",
                    tag: "span",
                    content: __$ctx.ctx.count
                } ]
            }, {
                elem: "list",
                tag: "ul",
                content: _$3jlist_content
            } ]
        } ]);
        return _$3jctx;
        return;
    }
    function $15(__$ctx) {
        var _$3gctx = [];
        var _$3glist_content = [];
        var _$3glist = __$ctx.ctx.list;
        for (var _$3gi = 0; _$3gi < _$3glist.length; _$3gi++) {
            var _$3gitem_content = [];
            if (_$3glist[_$3gi].title) {
                _$3gitem_content.push({
                    elem: "title",
                    content: {
                        tag: "span",
                        content: _$3glist[_$3gi].title
                    }
                });
            } else {
                undefined;
            }
            _$3gitem_content.push({
                elem: "body",
                content: _$3glist[_$3gi].content
            });
            _$3glist_content.push({
                elem: "item",
                elemMods: _$3glist[_$3gi].type ? {
                    type: _$3glist[_$3gi].type
                } : {},
                tag: "li",
                content: _$3gitem_content
            });
        }
        _$3gctx.push({
            block: "b-form",
            name: "filter",
            action: "#",
            content: {
                block: "b-filter",
                elem: "list",
                tag: "ul",
                content: _$3glist_content
            }
        });
        return _$3gctx;
        return;
    }
    function $20(__$ctx) {
        var _$3ectx = [];
        _$3ectx.push([ {
            elem: "slider"
        }, {
            elem: "info",
            content: [ {
                elem: "text",
                cls: "i-inline",
                content: "от"
            }, {
                elem: "value",
                elemMods: {
                    position: "min"
                },
                cls: "i-inline",
                value: __$ctx.ctx.values ? __$ctx.ctx.values[0] : __$ctx.ctx.min
            }, {
                elem: "text",
                cls: "i-inline",
                content: "до"
            }, {
                elem: "value",
                elemMods: {
                    position: "max"
                },
                cls: "i-inline",
                value: __$ctx.ctx.values ? __$ctx.ctx.values[1] : __$ctx.ctx.max
            } ]
        } ]);
        return _$3ectx;
        return;
    }
    function $29(__$ctx) {
        if (!(__$ctx.mods && __$ctx.mods.type === "radio") === false) {
            if (!!__$ctx.elem === false) {
                var _$29ctx = [];
                var _$29input_content = [];
                _$29input_content.push({
                    elem: "input",
                    attrs: {
                        checked: __$ctx.ctx.checked,
                        name: __$ctx.ctx.name ? __$ctx.ctx.name : __$ctx.generateId(),
                        type: "radio"
                    }
                });
                if (__$ctx.ctx.label) {
                    _$29ctx.push({
                        elem: "label",
                        elemMods: {
                            from: "radio"
                        },
                        content: [ _$29input_content, __$ctx.ctx.label ]
                    });
                } else {
                    _$29ctx.push(_$29input_content);
                }
                return _$29ctx;
                return;
            } else {
                return $35(__$ctx);
            }
        } else {
            return $35(__$ctx);
        }
    }
    function $35(__$ctx) {
        if (!(__$ctx.mods && __$ctx.mods.type === "textarea") === false) {
            if (!!__$ctx.elem === false) {
                return $38(__$ctx);
            } else {
                return $41(__$ctx);
            }
        } else {
            return $41(__$ctx);
        }
    }
    function $38(__$ctx) {
        var _$28ctx = [];
        var _$28attr = {};
        _$28attr.name = __$ctx.ctx.name ? __$ctx.ctx.name : __$ctx.generateId();
        if (__$ctx.ctx.placeholder) {
            _$28attr.placeholder = __$ctx.ctx.placeholder;
        } else {
            undefined;
        }
        if (__$ctx.ctx.label) {
            _$28ctx.push({
                elem: "label",
                elemMods: {
                    from: "text"
                },
                content: __$ctx.ctx.label
            });
        } else {
            undefined;
        }
        _$28ctx.push({
            elem: "textarea",
            attrs: _$28attr,
            content: __$ctx.ctx.value
        });
        if (__$ctx.ctx.help) {
            _$28ctx.push({
                elem: "help",
                elemMods: {
                    from: "text"
                },
                content: __$ctx.ctx.help
            });
        } else {
            undefined;
        }
        return _$28ctx;
        return;
    }
    function $41(__$ctx) {
        if (!(__$ctx.mods && __$ctx.mods.type === "text") === false) {
            if (!!__$ctx.elem === false) {
                return $44(__$ctx);
            } else {
                return $180(__$ctx);
            }
        } else {
            return $180(__$ctx);
        }
    }
    function $44(__$ctx) {
        var _$27ctx = [];
        var _$27attr = {
            type: "text"
        };
        _$27attr.name = __$ctx.ctx.name ? __$ctx.ctx.name : __$ctx.generateId();
        if (__$ctx.ctx.value) {
            _$27attr.value = __$ctx.ctx.value;
        } else {
            undefined;
        }
        if (__$ctx.ctx.placeholder) {
            _$27attr.placeholder = __$ctx.ctx.placeholder;
        } else {
            undefined;
        }
        if (__$ctx.ctx.label) {
            _$27ctx.push({
                elem: "label",
                elemMods: {
                    from: "text"
                },
                content: __$ctx.ctx.label
            });
        } else {
            undefined;
        }
        _$27ctx.push({
            elem: "input",
            attrs: _$27attr
        });
        if (__$ctx.ctx.help) {
            _$27ctx.push({
                elem: "help",
                elemMods: {
                    from: "text"
                },
                content: __$ctx.ctx.help
            });
        } else {
            undefined;
        }
        return _$27ctx;
        return;
    }
    function $51(__$ctx) {
        var _$36ctx = [];
        var _$36list_content = [];
        var _$36pages = [];
        for (var _$36i in __$ctx.ctx.pages) {
            var _$36number = __$ctx.ctx.pages[_$36i];
            _$36pages.push({
                elem: "item",
                elemMods: _$36number == __$ctx.ctx.current ? {
                    state: "current"
                } : {},
                mix: {
                    block: "i-inline"
                },
                tag: "li",
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-pagination",
                        elem: "link"
                    },
                    url: "?page=" + _$36number,
                    content: _$36number
                }
            });
        }
        if (__$ctx.ctx.current - 1 > 0) {
            _$36list_content.push([ {
                elem: "first",
                tag: "li",
                mix: {
                    block: "i-inline"
                },
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-pagination",
                        elem: "link"
                    },
                    url: "?page=1",
                    content: "1"
                }
            }, {
                elem: "separator",
                tag: "li",
                mix: {
                    block: "i-inline"
                },
                content: "..."
            } ]);
        } else {
            undefined;
        }
        _$36list_content.push(_$36pages);
        if (__$ctx.ctx.current + 1 < __$ctx.ctx.finish) {
            _$36list_content.push([ {
                elem: "separator",
                tag: "li",
                mix: {
                    block: "i-inline"
                },
                content: "..."
            }, {
                elem: "last",
                tag: "li",
                mix: {
                    block: "i-inline"
                },
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-pagination",
                        elem: "link"
                    },
                    url: "?page=" + __$ctx.ctx.finish,
                    content: __$ctx.ctx.finish
                }
            } ]);
        } else {
            undefined;
        }
        _$36ctx.push({
            elem: "list",
            tag: "ul",
            content: _$36list_content
        });
        return _$36ctx;
        return;
    }
    function $56(__$ctx) {
        var _$34ctx = [];
        var _$34inner_content = [];
        var _$34list_content = [];
        var _$34list = __$ctx.ctx.list;
        for (var _$34i = 0; _$34i < _$34list.length; _$34i++) {
            _$34list_content.push({
                elem: "item",
                tag: "li",
                cls: "i-inline",
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-category",
                        elem: "link"
                    },
                    url: _$34list[_$34i].url,
                    content: _$34list[_$34i].title
                }
            });
        }
        _$34inner_content.push([ {
            block: "b-link",
            mix: {
                block: "b-category",
                elem: "image"
            },
            url: __$ctx.ctx.url,
            content: {
                block: "b-icon",
                mix: {
                    block: "b-category",
                    elem: "img"
                },
                url: __$ctx.ctx.image,
                alt: __$ctx.ctx.title
            }
        }, {
            block: "b-link",
            mix: {
                block: "b-category",
                elem: "title"
            },
            url: __$ctx.ctx.url,
            content: __$ctx.ctx.title
        }, {
            elem: "list",
            tag: "ul",
            content: _$34list_content
        } ]);
        _$34ctx.push({
            elem: "inner",
            content: _$34inner_content
        });
        return _$34ctx;
        return;
    }
    function $61(__$ctx) {
        var _$32ctx = [];
        var _$32features_content = [];
        var _$32features = __$ctx.ctx.features;
        var _$32images_content = [];
        var _$32images = __$ctx.ctx.images;
        var _$32text_content = [];
        var _$32text = __$ctx.ctx.text.list;
        for (var _$32i = 0; _$32i < _$32features.length; _$32i++) {
            _$32features_content.push({
                elem: "feature",
                tag: "li",
                content: [ {
                    elem: "feature-title",
                    tag: "span",
                    content: _$32features[_$32i].title + ":"
                }, {
                    elem: "feature-value",
                    tag: "span",
                    content: _$32features[_$32i].value
                } ]
            });
        }
        for (var _$32i = 0; _$32i < _$32images.length; _$32i++) {
            _$32images_content.push({
                block: "b-gallery",
                elem: "item",
                mix: {
                    block: "b-card",
                    elem: "item"
                },
                tag: "li",
                images: {
                    preview: _$32images[_$32i].preview,
                    image: _$32images[_$32i].image,
                    full: _$32images[_$32i].full
                },
                title: __$ctx.ctx.title
            });
        }
        for (var _$32i = 0; _$32i < _$32text.length; _$32i++) {
            _$32text_content.push({
                elem: "text-item",
                elemMods: _$32i % 2 == 0 ? {
                    type: "even"
                } : {
                    type: "odd"
                },
                tag: "li",
                cls: "i-clearfix",
                content: [ {
                    elem: "text-image",
                    content: {
                        block: "b-icon",
                        mix: {
                            block: "b-card",
                            elem: "text-img"
                        },
                        url: _$32text[_$32i].image
                    }
                }, {
                    elem: "text-content",
                    content: _$32text[_$32i].content
                } ]
            });
        }
        _$32ctx.push([ {
            elem: "title",
            tag: "h1",
            content: __$ctx.ctx.title
        }, {
            elem: "article",
            content: "Арт. " + __$ctx.ctx.article
        }, {
            elem: "body",
            cls: "i-clearfix",
            content: [ {
                elem: "basket-block",
                content: [ {
                    elem: "basket-row",
                    content: [ {
                        elem: "price-block",
                        content: {
                            elem: "price",
                            cls: "i-inline",
                            content: __$ctx.ctx.price
                        }
                    }, {
                        block: "b-link",
                        mix: {
                            block: "b-card",
                            elem: "basket"
                        },
                        cls: "i-inline",
                        url: "#",
                        content: [ {
                            block: "b-card",
                            elem: "basket-ico",
                            cls: "i-inline"
                        }, {
                            elem: "text",
                            cls: "i-inline",
                            content: "В корзину"
                        } ]
                    } ]
                }, {
                    elem: "basket-row",
                    content: [ {
                        elem: "info",
                        elemMods: {
                            type: "delivery"
                        },
                        content: "Доставим этот товар бесплатно по Екатеринбургу"
                    }, {
                        elem: "info",
                        elemMods: {
                            type: "payment"
                        },
                        content: 'Оплата наличными при получении или удобным для вас способом:<br /><img src="../../images/temp/payment.png" />'
                    } ]
                }, {
                    elem: "basket-bottom"
                } ]
            }, {
                block: "b-gallery",
                mix: {
                    block: "b-card",
                    elem: "gallery"
                },
                js: {
                    fancybox: {
                        padding: 0
                    }
                },
                content: [ {
                    elem: "image",
                    mix: {
                        block: "b-card",
                        elem: "image"
                    },
                    image: _$32images[0].image,
                    full: _$32images[0].full
                }, {
                    block: "b-card",
                    elem: "image-carousel",
                    content: [ {
                        block: "b-link",
                        mix: {
                            block: "b-card",
                            elem: "arrow",
                            elemMods: {
                                position: "prev",
                                state: "disable"
                            }
                        },
                        url: "#"
                    }, {
                        block: "b-link",
                        mix: {
                            block: "b-card",
                            elem: "arrow",
                            elemMods: {
                                position: "next"
                            }
                        },
                        url: "#"
                    }, {
                        elem: "list",
                        tag: "ul",
                        content: _$32images_content
                    } ]
                } ]
            }, {
                elem: "main",
                content: [ {
                    block: "b-link",
                    mix: {
                        block: "b-card",
                        elem: "brand"
                    },
                    cls: "i-inline",
                    url: __$ctx.ctx.brand.url,
                    content: {
                        block: "b-icon",
                        mix: {
                            block: "b-card",
                            elem: "brand-image"
                        },
                        url: __$ctx.ctx.brand.image
                    }
                }, {
                    elem: "features",
                    tag: "ul",
                    content: _$32features_content
                }, {
                    elem: "discription",
                    content: __$ctx.ctx.discription
                } ]
            } ]
        } ]);
        if (__$ctx.ctx.related) {
            var _$32related_content = [];
            var _$32related_body = [];
            var _$32related = __$ctx.ctx.related;
            for (var _$32i = 0; _$32i < _$32related.length; _$32i++) {
                _$32related_content.push({
                    block: "b-related-product",
                    url: _$32related[_$32i].url,
                    title: _$32related[_$32i].title,
                    image: _$32related[_$32i].image,
                    brand: _$32related[_$32i].brand,
                    price: _$32related[_$32i].price
                });
            }
            _$32related_body.push([ {
                elem: "related-title",
                content: [ "Технические характеристики", {
                    elem: "related-count",
                    tag: "span",
                    content: _$32related.length
                } ]
            }, {
                elem: "related-list",
                content: _$32related_content
            } ]);
            if (_$32related.length > 3) {
                _$32related_body.push({
                    block: "b-link",
                    mix: {
                        block: "b-card",
                        elem: "related-all"
                    },
                    url: "#",
                    cls: "i-inline",
                    content: "Показать еще " + (_$32related.length - 3) + " товаров"
                });
            } else {
                undefined;
            }
            _$32ctx.push({
                elem: "related",
                content: _$32related_body
            });
        } else {
            undefined;
        }
        if (__$ctx.ctx.tech) {
            var _$32tech_content = [];
            var _$32tech = __$ctx.ctx.tech;
            for (var _$32i = 0; _$32i < _$32tech.length; _$32i++) {
                _$32tech_content.push({
                    elem: "tech-item",
                    elemMods: _$32i % 2 == 0 ? {
                        type: "even"
                    } : {
                        type: "odd"
                    },
                    tag: "li",
                    content: [ {
                        elem: "tech-text",
                        content: _$32tech[_$32i].title
                    }, {
                        elem: "tech-value",
                        content: _$32tech[_$32i].value
                    } ]
                });
            }
            _$32ctx.push({
                elem: "tech",
                content: [ {
                    elem: "tech-title",
                    tag: "h2",
                    content: "Технические характеристики"
                }, {
                    elem: "tech-list",
                    tag: "ul",
                    content: _$32tech_content
                } ]
            });
        } else {
            undefined;
        }
        _$32ctx.push({
            elem: "text",
            content: [ {
                elem: "text-title",
                tag: "h2",
                content: __$ctx.ctx.text.title
            }, {
                elem: "text-list",
                tag: "ul",
                content: _$32text_content
            } ]
        });
        return _$32ctx;
        return;
    }
    function $66(__$ctx) {
        var _$30ctx = [];
        _$30ctx.push([ {
            block: "b-link",
            mix: {
                block: "b-related-product",
                elem: "image"
            },
            url: __$ctx.ctx.url,
            content: {
                block: "b-icon",
                mix: {
                    block: "b-related-product",
                    elem: "img"
                },
                url: __$ctx.ctx.image,
                alt: __$ctx.ctx.title
            }
        }, {
            elem: "body",
            content: [ {
                block: "b-link",
                mix: {
                    block: "b-related-product",
                    elem: "title"
                },
                url: __$ctx.ctx.url,
                cls: "i-inline",
                content: __$ctx.ctx.title
            }, {
                elem: "brand",
                content: __$ctx.ctx.brand
            }, {
                elem: "bottom",
                content: [ {
                    elem: "price",
                    cls: "i-inline",
                    content: __$ctx.ctx.price
                }, {
                    block: "b-link",
                    mix: {
                        block: "b-related-product",
                        elem: "basket"
                    },
                    cls: "i-inline",
                    url: "#",
                    content: [ {
                        block: "b-related-product",
                        elem: "basket-ico",
                        cls: "i-inline"
                    }, {
                        elem: "text",
                        cls: "i-inline",
                        content: "В корзину"
                    } ]
                } ]
            } ]
        } ]);
        return _$30ctx;
        return;
    }
    function $71(__$ctx) {
        if (!__$ctx.ctx.images && !__$ctx.ctx.image && console) {
            console.error("Нет изображений для элемента галлереи");
        } else {
            undefined;
        }
        if (!__$ctx.ctx.images) {
            __$ctx.ctx.images = {
                preview: __$ctx.ctx.image
            };
        } else {
            undefined;
        }
        var _$2xpreview = __$ctx.ctx.images.preview;
        var _$2ximage = __$ctx.ctx.images.image || _$2xpreview;
        var _$2xfull = __$ctx.ctx.images.full || _$2ximage;
        var _$2xcontent = {
            block: "b-link",
            url: _$2xfull,
            mix: {
                block: "b-gallery",
                elem: "item-link"
            },
            mods: {
                pseudo: true
            },
            attrs: {
                "data-image": _$2ximage,
                "data-image-full": _$2xfull
            },
            content: {
                block: "b-icon",
                mix: {
                    block: "b-gallery",
                    elem: "preview"
                },
                url: _$2xpreview
            }
        };
        if (__$ctx.ctx.title) {
            _$2xcontent.title = __$ctx.ctx.title;
            _$2xcontent.content.alt = __$ctx.ctx.title;
        } else {
            undefined;
        }
        return _$2xcontent;
        return;
    }
    function $83(__$ctx) {
        var _$2sctx = [];
        var _$2slist_content = [];
        var _$2slist = __$ctx.ctx.list;
        for (var _$2si = 0; _$2si < _$2slist.length; _$2si++) {
            _$2slist_content.push({
                elem: "item",
                tag: "li",
                cls: "i-inline",
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-brand",
                        elem: "link"
                    },
                    url: _$2slist[_$2si].url,
                    content: {
                        block: "b-icon",
                        mix: {
                            block: "b-brand",
                            elem: "image"
                        },
                        url: _$2slist[_$2si].image
                    }
                }
            });
        }
        _$2sctx.push([ {
            elem: "list",
            tag: "ul",
            content: _$2slist_content
        }, {
            elem: "bottom",
            content: [ {
                block: "b-link",
                mix: {
                    block: "b-brand",
                    elem: "all"
                },
                url: __$ctx.ctx.url,
                content: {
                    elem: "text",
                    cls: "i-inline",
                    content: "Все бренды"
                }
            }, {
                elem: "count",
                tag: "span",
                content: __$ctx.ctx.count
            } ]
        } ]);
        return _$2sctx;
        return;
    }
    function $88(__$ctx) {
        var _$2rctx = [];
        var _$2rlist_content = [];
        var _$2rlist = __$ctx.ctx.list;
        for (var _$2ri = 0; _$2ri < _$2rlist.length; _$2ri++) {
            _$2rlist_content.push({
                elem: "item",
                tag: "li",
                cls: "i-inline",
                content: [ {
                    elem: "image",
                    cls: "i-inline",
                    content: {
                        block: "b-icon",
                        mix: {
                            block: "b-info",
                            elem: "img"
                        },
                        url: _$2rlist[_$2ri].image,
                        alt: _$2rlist[_$2ri].title
                    }
                }, {
                    elem: "body",
                    cls: "i-inline",
                    content: [ {
                        elem: "title",
                        content: _$2rlist[_$2ri].title
                    }, {
                        elem: "text",
                        content: _$2rlist[_$2ri].text
                    } ]
                } ]
            });
        }
        _$2rctx.push({
            elem: "list",
            tag: "ul",
            content: _$2rlist_content
        });
        return _$2rctx;
        return;
    }
    function $93(__$ctx) {
        var _$2qctx = [];
        var _$2qinner_content = [];
        _$2qinner_content.push([ {
            block: "b-link",
            mix: {
                block: "b-product",
                elem: "title"
            },
            url: __$ctx.ctx.url,
            content: {
                elem: "text",
                tag: "span",
                content: __$ctx.ctx.title
            }
        }, {
            elem: "category-block",
            content: {
                block: "b-link",
                mix: {
                    block: "b-product",
                    elem: "category"
                },
                url: __$ctx.ctx.categorylink,
                content: {
                    elem: "text",
                    cls: "i-inline",
                    content: __$ctx.ctx.category
                }
            }
        }, {
            elem: "image",
            content: [ {
                block: "b-icon",
                mix: {
                    block: "b-product",
                    elem: "img"
                },
                url: __$ctx.ctx.image,
                alt: __$ctx.ctx.title
            }, {
                elem: "brand-block",
                content: {
                    block: "b-icon",
                    mix: {
                        block: "b-product",
                        elem: "brand"
                    },
                    url: __$ctx.ctx.brand
                }
            }, {
                block: "b-link",
                mix: {
                    block: "b-product",
                    elem: "simile"
                },
                url: "#",
                content: {
                    elem: "text",
                    cls: "i-inline",
                    content: "Сравнить"
                }
            } ]
        }, {
            elem: "bottom",
            content: [ {
                elem: "price",
                cls: "i-inline",
                content: __$ctx.ctx.price
            }, {
                block: "b-link",
                mix: {
                    block: "b-product",
                    elem: "basket"
                },
                cls: "i-inline",
                url: "#",
                content: [ {
                    block: "b-product",
                    elem: "basket-ico",
                    cls: "i-inline"
                }, {
                    elem: "text",
                    cls: "i-inline",
                    content: "В корзину"
                } ]
            } ]
        } ]);
        if (__$ctx.ctx.oldprice) {
            _$2qinner_content.push({
                elem: "oldprice",
                cls: "i-inline",
                content: __$ctx.ctx.oldprice + " Р"
            });
        } else {
            undefined;
        }
        _$2qctx.push({
            elem: "inner",
            content: _$2qinner_content
        });
        return _$2qctx;
        return;
    }
    function $98(__$ctx) {
        var _$2nctx = [];
        _$2nctx.push([ {
            elem: "top",
            content: [ {
                elem: "title",
                cls: "i-inline clickme",
                content: __$ctx.ctx.title
            }, {
                elem: "toggle-block",
                cls: "i-inline",
                content: [ {
                    elem: "toggle-title",
                    cls: "i-inline clickme",
                    content: "Показать:"
                }, {
                    elem: "toggle-text",
                    elemMods: {
                        state: "active",
                        position: "min"
                    },
                    cls: "i-inline",
                    content: "0"
                }, {
                    elem: "toggle",
                    mix: {
                        block: "i-inline"
                    },
                    cls: "light",
                    attrs: {
                        "data-checkbox": "checkme",
                        rel: "clickme"
                    }
                }, {
                    block: "b-form-control",
                    elem: "input",
                    cls: "checkme",
                    attrs: {
                        type: "checkbox",
                        disabled: "disabled"
                    }
                }, {
                    elem: "toggle-text",
                    cls: "i-inline",
                    elemMods: {
                        position: "max"
                    },
                    content: "Все"
                } ]
            } ]
        }, {
            elem: "body",
            content: {
                elem: "inner",
                content: __$ctx.ctx.content
            }
        } ]);
        return _$2nctx;
        return;
    }
    function $103(__$ctx) {
        var _$2ictx = [];
        _$2ictx.push({
            block: "b-container",
            content: {
                block: "b-footer",
                elem: "wrapper",
                cls: "i-clearfix",
                content: [ {
                    elem: "logo",
                    content: {
                        block: "b-link",
                        mix: {
                            block: "b-footer",
                            elem: "logo-link"
                        },
                        url: "/",
                        content: {
                            block: "b-icon",
                            mix: {
                                block: "b-footer",
                                elem: "logo-image"
                            },
                            url: "../../images/design/logo-footer.png",
                            alt: "Автогаджет"
                        }
                    }
                }, {
                    elem: "contact",
                    content: [ {
                        elem: "phone",
                        cls: "i-inline",
                        content: [ {
                            elem: "index",
                            tag: "span",
                            content: "+7 (343) "
                        }, "268–33–83" ]
                    }, {
                        block: "b-link",
                        mix: {
                            block: "b-footer",
                            elem: "mail"
                        },
                        url: "mailto:info@ampcar.ru",
                        cls: "i-inline",
                        content: "info@ampcar.ru"
                    } ]
                }, {
                    elem: "inner",
                    content: {
                        block: "b-menu",
                        mods: {
                            position: "footer",
                            type: "horiz"
                        },
                        list: [ {
                            url: "#",
                            title: "Каталог"
                        }, {
                            url: "#",
                            title: "Бренды"
                        }, {
                            url: "#",
                            title: "Оплата и доставка"
                        }, {
                            url: "#",
                            title: "Статьи"
                        }, {
                            url: "#",
                            title: "Контакты"
                        } ]
                    }
                } ]
            }
        });
        return _$2ictx;
        return;
    }
    function $108(__$ctx) {
        var _$2hctx = [];
        _$2hctx.push({
            block: "b-form",
            mix: {
                block: "b-order-form",
                elem: "form"
            },
            action: "#",
            method: "post",
            name: "order",
            content: [ {
                block: "b-debug-toolbar"
            }, {
                elem: "fieldset",
                content: [ {
                    elem: "legend",
                    mix: {
                        block: "b-order-form",
                        elem: "legend"
                    },
                    content: "Оформление заказа"
                }, {
                    elem: "info",
                    mix: {
                        block: "b-order-form",
                        elem: "info"
                    },
                    content: " – обязательные поля"
                }, {
                    elem: "row",
                    content: {
                        block: "b-form-control",
                        mods: {
                            type: "text"
                        },
                        mix: {
                            block: "b-order-form",
                            elem: "required"
                        },
                        label: "Представьтесь"
                    }
                }, {
                    elem: "row",
                    content: {
                        block: "b-form-control",
                        mods: {
                            type: "text"
                        },
                        mix: [ {
                            block: "b-order-form",
                            elem: "required"
                        }, {
                            block: "b-order-form",
                            elem: "phone"
                        } ],
                        label: "Номер телефона",
                        placeholder: "8(___) ___-__-__"
                    }
                }, {
                    elem: "row",
                    content: [ {
                        block: "b-order-form",
                        elem: "title",
                        content: "Доставка"
                    }, {
                        block: "b-form-control",
                        mods: {
                            type: "radio"
                        },
                        name: "delivery",
                        cls: "i-inline",
                        checked: "checked",
                        label: [ "По Екатеринбургу", {
                            block: "b-order-form",
                            elem: "label-info",
                            tag: "span",
                            content: "(курьером)"
                        } ]
                    }, {
                        block: "b-form-control",
                        mods: {
                            type: "radio"
                        },
                        name: "delivery",
                        cls: "i-inline",
                        label: [ "По России", {
                            block: "b-order-form",
                            elem: "label-info",
                            tag: "span",
                            content: "(почтой России)"
                        } ]
                    } ]
                }, {
                    elem: "row",
                    content: [ {
                        block: "b-form-control",
                        mods: {
                            type: "text"
                        },
                        mix: {
                            block: "b-order-form",
                            elem: "required"
                        },
                        cls: "i-inline",
                        label: "Улица"
                    }, {
                        block: "b-form-control",
                        mods: {
                            type: "text"
                        },
                        mix: [ {
                            block: "b-order-form",
                            elem: "required"
                        }, {
                            block: "b-order-form",
                            elem: "small"
                        } ],
                        cls: "i-inline",
                        label: "Дом"
                    }, {
                        block: "b-form-control",
                        mods: {
                            type: "text"
                        },
                        mix: {
                            block: "b-order-form",
                            elem: "small"
                        },
                        cls: "i-inline",
                        label: "Квартира/офис"
                    } ]
                }, {
                    elem: "row",
                    content: [ {
                        block: "b-form-control",
                        mods: {
                            type: "text"
                        },
                        mix: [ {
                            block: "b-order-form",
                            elem: "required"
                        }, {
                            block: "b-order-form",
                            elem: "date"
                        } ],
                        cls: "i-inline",
                        label: "Дата доставки"
                    }, {
                        block: "b-order-form",
                        elem: "time",
                        cls: "i-inline",
                        content: [ {
                            block: "b-order-form",
                            elem: "title",
                            content: "Время доставки"
                        }, {
                            block: "b-form-control",
                            mods: {
                                type: "radio"
                            },
                            name: "time",
                            cls: "i-inline",
                            checked: "checked",
                            label: "Утром"
                        }, {
                            block: "b-form-control",
                            mods: {
                                type: "radio"
                            },
                            name: "time",
                            cls: "i-inline",
                            label: "Вечером"
                        } ]
                    } ]
                }, {
                    elem: "row",
                    content: [ {
                        block: "b-order-form",
                        elem: "title",
                        content: "Оплата"
                    }, {
                        block: "b-form-control",
                        mods: {
                            type: "radio"
                        },
                        name: "payment",
                        cls: "i-inline",
                        checked: "checked",
                        label: "Наличными при получени"
                    }, {
                        block: "b-form-control",
                        mods: {
                            type: "radio"
                        },
                        name: "payment",
                        cls: "i-inline",
                        label: "Электронный платеж"
                    } ]
                }, {
                    elem: "row",
                    content: {
                        block: "b-form-control",
                        mods: {
                            type: "textarea"
                        },
                        label: "Комментарий к заказу"
                    }
                } ]
            }, {
                elem: "fieldset",
                content: {
                    block: "b-form-control",
                    elem: "input",
                    mix: {
                        block: "b-order-form",
                        elem: "submit"
                    },
                    attrs: {
                        type: "submit",
                        value: "Оформить заказ"
                    }
                }
            } ]
        });
        return _$2hctx;
        return;
    }
    function $121(__$ctx) {
        var _$26ctx = [];
        _$26ctx.push([ {
            block: "b-link",
            mix: {
                block: "b-order-item",
                elem: "image"
            },
            cls: "i-inline",
            url: __$ctx.ctx.url,
            content: {
                block: "b-icon",
                mix: {
                    block: "b-order-item",
                    elem: "img"
                },
                url: __$ctx.ctx.image,
                alt: __$ctx.ctx.title
            }
        }, {
            elem: "price-block",
            content: [ {
                elem: "price",
                cls: "i-inline",
                content: __$ctx.ctx.price
            }, {
                block: "b-link",
                mix: {
                    block: "b-order-item",
                    elem: "del"
                },
                url: "#"
            } ]
        }, {
            elem: "count",
            content: {
                block: "b-order-count",
                min: 1,
                value: __$ctx.ctx.count
            }
        }, {
            elem: "main",
            content: [ {
                block: "b-link",
                mix: {
                    block: "b-order-item",
                    elem: "title"
                },
                url: __$ctx.ctx.url,
                content: __$ctx.ctx.title
            }, {
                elem: "brand-block",
                cls: "i-inline",
                content: {
                    block: "b-icon",
                    mix: {
                        block: "b-order-item",
                        elem: "brand"
                    },
                    url: __$ctx.ctx.brand
                }
            } ]
        } ]);
        return _$26ctx;
        return;
    }
    function $126(__$ctx) {
        var _$23ctx = [];
        _$23ctx.push([ {
            block: "b-link",
            mix: {
                block: "b-order-count",
                elem: "link",
                elemMods: __$ctx.ctx.value == __$ctx.ctx.min ? {
                    type: "less",
                    state: "disable"
                } : {
                    type: "less"
                }
            },
            cls: "i-btn",
            url: "#",
            content: __$ctx.ctx.less
        }, {
            block: "b-link",
            mix: {
                block: "b-order-count",
                elem: "link",
                elemMods: {
                    type: "more"
                }
            },
            cls: "i-btn",
            url: "#",
            content: __$ctx.ctx.more
        }, {
            block: "b-form-control",
            elem: "input",
            mix: {
                block: "b-order-count",
                elem: "input"
            },
            attrs: {
                type: "text",
                value: __$ctx.ctx.value ? __$ctx.ctx.value : 0,
                name: __$ctx.ctx.name ? __$ctx.ctx.name : "count"
            }
        } ]);
        return _$23ctx;
        return;
    }
    function $136(__$ctx) {
        var _$1zctx = [];
        var _$1zlist_content = [];
        _$1zlist_content.push({
            elem: "item",
            tag: "li",
            mix: {
                block: "i-inline"
            },
            content: [ {
                block: "b-link",
                mix: [ {
                    block: "b-pathway",
                    elem: "link",
                    elemMods: {
                        type: "home"
                    }
                }, {
                    block: "i-inline"
                } ],
                url: "/"
            } ]
        });
        if (__$ctx.ctx.list) {
            var _$1zlist = __$ctx.ctx.list;
            for (var _$1zi = 0; _$1zi < _$1zlist.length; _$1zi++) {
                _$1zlist_content.push({
                    elem: "item",
                    tag: "li",
                    mix: {
                        block: "i-inline"
                    },
                    content: [ {
                        elem: "separator",
                        tag: "span",
                        mix: {
                            block: "i-inline"
                        },
                        content: "­­­­­­­­­→"
                    }, {
                        block: "b-link",
                        mix: [ {
                            block: "b-pathway",
                            elem: "link"
                        }, {
                            block: "i-inline"
                        } ],
                        url: _$1zlist[_$1zi].url,
                        content: _$1zlist[_$1zi].title
                    } ]
                });
            }
        } else {
            undefined;
        }
        _$1zctx.push({
            elem: "list",
            mix: {
                block: "i-inline"
            },
            tag: "ul",
            content: _$1zlist_content
        });
        if (__$ctx.ctx.title) {
            _$1zctx.push([ {
                elem: "separator",
                tag: "span",
                mix: {
                    block: "i-inline"
                },
                content: "­­­­­­­­­→"
            }, {
                elem: "title",
                tag: "span",
                mix: {
                    block: "i-inline"
                },
                content: __$ctx.ctx.title
            } ]);
        } else {
            undefined;
        }
        return _$1zctx;
        return;
    }
    function $141(__$ctx) {
        var _$1yctx = [];
        var _$1ylist_content = [];
        var _$1ylist = __$ctx.ctx.list;
        for (var _$1yi = 0; _$1yi < _$1ylist.length; _$1yi++) {
            _$1ylist_content.push({
                elem: "item",
                elemMods: _$1ylist[_$1yi].type ? {
                    type: _$1ylist[_$1yi].type
                } : {},
                tag: "li",
                cls: "i-inline",
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-hub",
                        elem: "link"
                    },
                    url: _$1ylist[_$1yi].url,
                    content: {
                        elem: "text",
                        cls: "i-inline",
                        content: _$1ylist[_$1yi].title
                    }
                }
            });
        }
        _$1yctx.push([ {
            block: "b-link",
            mix: {
                block: "b-hub",
                elem: "all"
            },
            url: "#",
            content: {
                elem: "text",
                cls: "i-inline",
                content: "Все"
            }
        }, {
            elem: "wrapper",
            content: {
                elem: "list",
                tag: "ul",
                content: _$1ylist_content
            }
        } ]);
        return _$1yctx;
        return;
    }
    function $146(__$ctx) {
        var _$1wctx = [];
        var _$1wlist_content = [];
        if (__$ctx.ctx.list) {
            var _$1wlist = __$ctx.ctx.list;
            for (var _$1wi = 0; _$1wi < _$1wlist.length; _$1wi++) {
                var _$1witem_content = [];
                _$1witem_content.push({
                    block: "b-link",
                    mix: {
                        block: "b-great-menu",
                        elem: "link"
                    },
                    url: _$1wlist[_$1wi].url,
                    content: {
                        elem: "text",
                        cls: "i-inline",
                        content: _$1wlist[_$1wi].title
                    }
                });
                if (_$1wlist[_$1wi].list) {
                    var _$1wdroplist = _$1wlist[_$1wi].list;
                    var _$1wdrop_content = [];
                    for (var _$1wj = 0; _$1wj < _$1wdroplist.length; _$1wj++) {
                        var _$1wbody_content = [];
                        _$1wbody_content.push({
                            block: "b-link",
                            mix: {
                                block: "b-great-menu",
                                elem: "droplink"
                            },
                            url: _$1wdroplist[_$1wj].url,
                            content: {
                                elem: "text",
                                cls: "i-inline",
                                content: _$1wdroplist[_$1wj].title
                            }
                        });
                        if (_$1wdroplist[_$1wj].list) {
                            var _$1wsublist = _$1wdroplist[_$1wj].list;
                            var _$1wsubmenu_content = [];
                            for (var _$1wk = 0; _$1wk < _$1wsublist.length; _$1wk++) {
                                _$1wsubmenu_content.push({
                                    elem: "subitem",
                                    tag: "li",
                                    cls: "i-inline",
                                    content: {
                                        block: "b-link",
                                        mix: {
                                            block: "b-great-menu",
                                            elem: "sublink"
                                        },
                                        url: _$1wsublist[_$1wk].url,
                                        content: {
                                            elem: "text",
                                            cls: "i-inline",
                                            content: _$1wsublist[_$1wk].title
                                        }
                                    }
                                });
                            }
                            _$1wbody_content.push({
                                elem: "sublist",
                                tag: "ul",
                                content: _$1wsubmenu_content
                            });
                        } else {
                            undefined;
                        }
                        _$1wdrop_content.push({
                            elem: "dropitem",
                            tag: "li",
                            cls: "i-inline i-clearfix",
                            content: [ {
                                block: "b-link",
                                mix: {
                                    block: "b-great-menu",
                                    elem: "image"
                                },
                                url: _$1wdroplist[_$1wj].url,
                                content: {
                                    block: "b-icon",
                                    mix: {
                                        block: "b-great-menu",
                                        elem: "img"
                                    },
                                    url: _$1wdroplist[_$1wj].image,
                                    alt: _$1wdroplist[_$1wj].title
                                }
                            }, {
                                elem: "body",
                                content: _$1wbody_content
                            } ]
                        });
                    }
                    _$1witem_content.push({
                        elem: "dropmenu",
                        tag: "ul",
                        content: _$1wdrop_content
                    });
                } else {
                    undefined;
                }
                _$1wlist_content.push({
                    elem: "item",
                    elemMods: _$1wlist[_$1wi].type ? {
                        type: _$1wlist[_$1wi].type
                    } : {},
                    tag: "li",
                    cls: "i-inline",
                    content: _$1witem_content
                });
            }
        } else {
            undefined;
        }
        _$1wctx.push({
            elem: "list",
            tag: "ul",
            content: _$1wlist_content
        });
        return _$1wctx;
        return;
    }
    function $151(__$ctx) {
        var _$1uctx = [];
        var _$1uwrapper_content = [];
        _$1uwrapper_content.push({
            block: "b-container",
            content: {
                block: "b-header",
                elem: "wrapper",
                cls: "i-clearfix",
                content: [ {
                    elem: "logo",
                    content: {
                        block: "b-link",
                        mix: {
                            block: "b-header",
                            elem: "logo-link"
                        },
                        url: "/",
                        content: {
                            block: "b-icon",
                            mix: {
                                block: "b-header",
                                elem: "logo-image"
                            },
                            url: "../../images/design/logo.png",
                            alt: "Автогаджет"
                        }
                    }
                }, {
                    elem: "contact",
                    content: [ {
                        elem: "feedback",
                        content: {
                            block: "b-feedback",
                            url: "#"
                        }
                    }, {
                        elem: "phone",
                        content: [ {
                            elem: "index",
                            tag: "span",
                            content: "+7 (343)"
                        }, "268–33–83" ]
                    }, {
                        block: "b-basket",
                        mods: {
                            state: "active"
                        },
                        list: [ "2990", "3450", "2990", "3450" ]
                    } ]
                }, {
                    elem: "inner",
                    content: [ {
                        block: "b-menu",
                        mods: {
                            type: "horiz",
                            position: "top"
                        },
                        list: [ {
                            url: "#",
                            title: "Оплата и доставка"
                        }, {
                            url: "#",
                            title: "Статьи"
                        }, {
                            url: "#",
                            title: "Контакты"
                        } ]
                    }, {
                        block: "b-search-form",
                        placeholder: "Поиск по товарам",
                        button: "&nbsp"
                    }, {
                        elem: "search-info",
                        content: [ "Например, ", {
                            block: "b-link",
                            mix: {
                                block: "b-header",
                                elem: "search-link"
                            },
                            url: "#",
                            content: {
                                elem: "text",
                                cls: "i-inline",
                                content: "автосигнализация StarLine A91"
                            }
                        } ]
                    } ]
                } ]
            }
        });
        _$1uctx.push({
            elem: "scroll-block",
            content: _$1uwrapper_content
        });
        return _$1uctx;
        return;
    }
    function $161(__$ctx) {
        var _$1rctx = [];
        if (__$ctx.ctx.list) {
            var _$1rcount = 0;
            var _$1rsumma = 0;
            var _$1rlist = __$ctx.ctx.list;
            for (var _$1ri = 0; _$1ri < _$1rlist.length; _$1ri++) {
                _$1rcount = _$1rcount + 1;
                _$1rsumma = _$1rsumma + parseInt(_$1rlist[_$1ri]);
            }
            _$1rctx.push([ {
                block: "b-link",
                mix: {
                    block: "b-basket",
                    elem: "link"
                },
                url: "#",
                content: {
                    elem: "text",
                    cls: "i-inline",
                    content: "В корзине:"
                }
            }, {
                elem: "text",
                tag: "span",
                content: _$1rcount + ' тов. на сумму <span class="b-basket__price i-inline"> ' + _$1rsumma + "</span>"
            } ]);
        } else {
            _$1rctx.push({
                elem: "text",
                tag: "span",
                content: "Корзина пуста"
            });
        }
        return _$1rctx;
        return;
    }
    function $166(__$ctx) {
        var _$1pbutton_content = "";
        var _$1pinput_attr = {
            type: "search"
        };
        if (__$ctx.ctx.placeholder) {
            _$1pinput_attr.placeholder = __$ctx.ctx.placeholder;
        } else {
            undefined;
        }
        if (__$ctx.ctx.value) {
            _$1pinput_attr.value = __$ctx.ctx.value;
        } else {
            undefined;
        }
        if (__$ctx.ctx.button) {
            var _$1pbutton_content = __$ctx.ctx.button;
        } else {
            undefined;
        }
        var _$1pc = {
            block: "b-form",
            mods: {
                type: "search"
            },
            content: {
                block: "b-form-control",
                content: {
                    elem: "append",
                    content: [ {
                        elem: "input",
                        elemMods: {
                            type: "search-query"
                        },
                        attrs: _$1pinput_attr
                    }, {
                        elem: "button",
                        cls: "i-btn",
                        content: _$1pbutton_content
                    } ]
                }
            }
        };
        return _$1pc;
        return;
    }
    function $171(__$ctx) {
        var _$1cctx = [];
        var _$1clist_content = [];
        var _$1clist = __$ctx.ctx.list;
        for (var _$1ci = 0; _$1ci < _$1clist.length; _$1ci++) {
            _$1clist_content.push({
                elem: "item",
                elemMods: _$1clist[_$1ci].type ? {
                    type: _$1clist[_$1ci].type
                } : {},
                tag: "li",
                content: {
                    block: "b-link",
                    mix: {
                        block: "b-menu",
                        elem: "link"
                    },
                    cls: "i-inline",
                    url: _$1clist[_$1ci].url,
                    content: {
                        block: "b-menu",
                        elem: "title",
                        tag: "span",
                        cls: "i-inline",
                        content: _$1clist[_$1ci].title
                    }
                }
            });
        }
        _$1cctx.push({
            elem: "list",
            tag: "ul",
            content: _$1clist_content
        });
        return _$1cctx;
        return;
    }
    function $180(__$ctx) {
        return __$ctx.ctx.content;
        return;
    }
    function $181(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-catalog") {
            if (!!__$ctx.elem === false) {
                return "i-inline i-clearfix";
                return;
            } else {
                return $218(__$ctx);
            }
        } else if (__t === "b-category") {
            if (!!__$ctx.elem === false) {
                return "i-inline";
                return;
            } else {
                return $218(__$ctx);
            }
        } else if (__t === "b-related-product") {
            if (!!__$ctx.elem === false) {
                return "i-inline i-clearfix";
                return;
            } else {
                return $218(__$ctx);
            }
        } else if (__t === "b-product") {
            if (!!__$ctx.elem === false) {
                return "i-inline";
                return;
            } else {
                return $218(__$ctx);
            }
        } else if (__t === "b-order-item") {
            if (!!__$ctx.elem === false) {
                return "i-clearfix";
                return;
            } else {
                return $218(__$ctx);
            }
        } else if (__t === "b-basket") {
            if (!!__$ctx.elem === false) {
                return "i-inline";
                return;
            } else {
                return $218(__$ctx);
            }
        } else if (__t === "b-page") {
            if (__$ctx.elem === "root") {
                return "i-ua_js_no i-ua_css_standard";
                return;
            } else {
                return $218(__$ctx);
            }
        } else {
            return $218(__$ctx);
        }
    }
    function $218(__$ctx) {
        return undefined;
        return;
    }
    function $219(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-filter" || __t === "b-gallery" || __t === "b-product" || __t === "b-product-list" || __t === "b-related-product" || __t === "b-debug-toolbar" || __t === "b-order-item") {
            if (!!__$ctx.elem === false) {
                return true;
                return;
            } else {
                return $306(__$ctx);
            }
        } else if (__t === "b-order-count") {
            if (!!__$ctx.elem === false) {
                return {
                    min: __$ctx.ctx.min,
                    max: __$ctx.ctx.max,
                    step: __$ctx.ctx.step
                };
                return;
            } else {
                return $306(__$ctx);
            }
        } else if (__t === "b-order" || __t === "b-hub" || __t === "b-card" || __t === "b-great-menu" || __t === "b-products" || __t === "b-header") {
            if (!!__$ctx.elem === false) {
                return true;
                return;
            } else {
                return $306(__$ctx);
            }
        } else if (__t === "b-range") {
            if (!!__$ctx.elem === false) {
                return {
                    min: __$ctx.ctx.min,
                    max: __$ctx.ctx.max,
                    step: __$ctx.ctx.step
                };
                return;
            } else {
                return $306(__$ctx);
            }
        } else if (__t === "b-form-control" || __t === "b-order-form") {
            if (!!__$ctx.elem === false) {
                return true;
                return;
            } else {
                return $306(__$ctx);
            }
        } else {
            return $306(__$ctx);
        }
    }
    function $306(__$ctx) {
        return undefined;
        return;
    }
    function $307(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-range") {
            if (__$ctx.elem === "value") {
                var _$3cattrs = __$ctx.ctx.attrs || {};
                _$3cattrs.value = __$ctx.ctx.value;
                _$3cattrs.type = "text";
                return _$3cattrs;
                return;
            } else {
                return $359(__$ctx);
            }
        } else if (__t === "b-scripts") {
            if (__$ctx.elem === "js") {
                if (!__$ctx.ctx.url === false) {
                    return {
                        src: __$ctx.ctx.url
                    };
                    return;
                } else {
                    return $359(__$ctx);
                }
            } else {
                return $359(__$ctx);
            }
        } else if (__t === "b-debug-toolbar") {
            if (__$ctx.elem === "action") {
                return {
                    "data-action": __$ctx.ctx.action
                };
                return;
            } else {
                return $359(__$ctx);
            }
        } else if (__t === "b-form") {
            if (!!__$ctx.elem === false) {
                return {
                    action: __$ctx.ctx.action,
                    method: __$ctx.ctx.method,
                    name: __$ctx.ctx.name
                };
                return;
            } else {
                return $359(__$ctx);
            }
        } else if (__t === "b-icon") {
            if (!!__$ctx.elem === false) {
                var _$1bctx = __$ctx.ctx, _$1ba = {
                    src: "//yandex.st/lego/_/La6qi18Z8LwgnZdsAr1qy1GwCwo.gif",
                    alt: ""
                }, _$1bprops = [ "alt", "width", "height" ], _$1bp;
                _$1bctx.url && (_$1ba.src = _$1bctx.url);
                while (_$1bp = _$1bprops.shift()) {
                    _$1bctx[_$1bp] && (_$1ba[_$1bp] = _$1bctx[_$1bp]);
                }
                return _$1ba;
                return;
            } else {
                return $359(__$ctx);
            }
        } else if (__t === "b-link") {
            if (!!__$ctx.elem === false) {
                return $338(__$ctx);
            } else {
                return $359(__$ctx);
            }
        } else if (__t === "b-page") {
            var __t = __$ctx.elem;
            if (__t === "js") {
                if (!__$ctx.ctx.url === false) {
                    return {
                        src: __$ctx.ctx.url
                    };
                    return;
                } else {
                    return $359(__$ctx);
                }
            } else if (__t === "css") {
                if (!__$ctx.ctx.url === false) {
                    return {
                        rel: "stylesheet",
                        href: __$ctx.ctx.url
                    };
                    return;
                } else {
                    return $359(__$ctx);
                }
            } else if (__t === "favicon") {
                return {
                    rel: "shortcut icon",
                    href: __$ctx.ctx.url
                };
                return;
            } else if (__t === "meta") {
                return __$ctx.ctx.attrs;
                return;
            } else {
                return $359(__$ctx);
            }
        } else {
            return $359(__$ctx);
        }
    }
    function $338(__$ctx) {
        var __r0, __r1, __r2, __r3;
        var _$18ctx = __$ctx.ctx, _$18props = [ "title", "target" ], _$18p = typeof _$18ctx.url, _$18a = {
            href: _$18p === "undefined" || _$18p === "string" ? _$18ctx.url : (_$18p = [], "", __r0 = __$ctx._buf, __$ctx._buf = _$18p, __r1 = __$ctx._mode, __$ctx._mode = "", __r2 = __$ctx.ctx, __$ctx.ctx = _$18ctx.url, __r3 = $536(__$ctx), __$ctx._buf = __r0, __$ctx._mode = __r1, __$ctx.ctx = __r2, "", __r3, _$18p.join(""))
        };
        while (_$18p = _$18props.pop()) {
            _$18ctx[_$18p] && (_$18a[_$18p] = _$18ctx[_$18p]);
        }
        return _$18a;
        return;
    }
    function $359(__$ctx) {
        return undefined;
        return;
    }
    function $360(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-range") {
            if (__$ctx.elem === "value") {
                return "input";
                return;
            } else {
                return $443(__$ctx);
            }
        } else if (__t === "b-scripts") {
            if (__$ctx.elem === "js") {
                return "script";
                return;
            } else {
                return $443(__$ctx);
            }
        } else if (__t === "b-debug-toolbar") {
            if (__$ctx.elem === "action") {
                return "li";
                return;
            } else {
                return $443(__$ctx);
            }
        } else if (__t === "b-form-control") {
            var __t = __$ctx.elem;
            if (__t === "button") {
                return "button";
                return;
            } else if (__t === "textarea") {
                return "textarea";
                return;
            } else if (__t === "help") {
                return "span";
                return;
            } else if (__t === "input") {
                return "input";
                return;
            } else if (__t === "label") {
                return "label";
                return;
            } else {
                return $443(__$ctx);
            }
        } else if (__t === "b-form") {
            var __t = __$ctx.elem;
            if (__t === "legend") {
                return "legend";
                return;
            } else if (__t === "fieldset") {
                return "fieldset";
                return;
            } else if (__t === "error") {
                return "li";
                return;
            } else if (__t === "errors") {
                return "ul";
                return;
            } else {
                if (!!__$ctx.elem === false) {
                    return "form";
                    return;
                } else {
                    return $443(__$ctx);
                }
            }
        } else if (__t === "b-icon") {
            if (!!__$ctx.elem === false) {
                return "img";
                return;
            } else {
                return $443(__$ctx);
            }
        } else if (__t === "b-link") {
            if (__$ctx.elem === "text") {
                return "span";
                return;
            } else {
                if (!!__$ctx.elem === false) {
                    return "a";
                    return;
                } else {
                    return $443(__$ctx);
                }
            }
        } else if (__t === "b-page") {
            var __t = __$ctx.elem;
            if (__t === "js") {
                return "script";
                return;
            } else if (__t === "css") {
                if (!__$ctx.ctx.url === false) {
                    return "link";
                    return;
                } else {
                    return "style";
                    return;
                }
            } else if (__t === "favicon") {
                return "link";
                return;
            } else if (__t === "meta") {
                return "meta";
                return;
            } else if (__t === "head") {
                return "head";
                return;
            } else if (__t === "root") {
                return "html";
                return;
            } else {
                if (!!__$ctx.elem === false) {
                    return "body";
                    return;
                } else {
                    return $443(__$ctx);
                }
            }
        } else if (__t === "i-ua") {
            if (!!__$ctx.elem === false) {
                return "script";
                return;
            } else {
                return $443(__$ctx);
            }
        } else {
            return $443(__$ctx);
        }
    }
    function $443(__$ctx) {
        return undefined;
        return;
    }
    function $444(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-gallery") {
            if (__$ctx.elem === "item") {
                var _$2wmix = [];
                _$2wmix.push({
                    block: "i-inline"
                });
                __$ctx.isFirst() && _$2wmix.push({
                    elemMods: {
                        state: "active"
                    }
                });
                return _$2wmix;
                return;
            } else {
                return $456(__$ctx);
            }
        } else if (__t === "b-page") {
            if (!!__$ctx.elem === false) {
                return [ {
                    elem: "body"
                } ];
                return;
            } else {
                return $456(__$ctx);
            }
        } else {
            return $456(__$ctx);
        }
    }
    function $456(__$ctx) {
        return undefined;
        return;
    }
    function $457(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-scripts") {
            if (__$ctx.elem === "js") {
                return false;
                return;
            } else {
                return $484(__$ctx);
            }
        } else if (__t === "b-page") {
            var __t = __$ctx.elem;
            if (__t === "js" || __t === "css" || __t === "favicon" || __t === "meta" || __t === "head" || __t === "root") {
                return false;
                return;
            } else {
                return $484(__$ctx);
            }
        } else if (__t === "i-ua") {
            if (!!__$ctx.elem === false) {
                return false;
                return;
            } else {
                return $484(__$ctx);
            }
        } else {
            return $484(__$ctx);
        }
    }
    function $484(__$ctx) {
        return undefined;
        return;
    }
    function $493(__$ctx) {
        var __t = __$ctx.block;
        if (__t === "b-page") {
            if (__$ctx.elem === "css") {
                if (!__$ctx.ctx.hasOwnProperty("ie") === false) {
                    if (!!__$ctx.ctx._ieCommented === false) {
                        return $498(__$ctx);
                    } else {
                        return $503(__$ctx);
                    }
                } else {
                    return $503(__$ctx);
                }
            } else {
                return $503(__$ctx);
            }
        } else if (__t === "i-jquery") {
            if (__$ctx.elem === "core") {
                var __r0, __r1, __r2;
                return "", __r0 = __$ctx._mode, __$ctx._mode = "", __r1 = __$ctx.ctx, __$ctx.ctx = {
                    block: "b-page",
                    elem: "js",
                    url: "//yandex.st/jquery/1.7.2/jquery.min.js"
                }, __r2 = $536(__$ctx), __$ctx._mode = __r0, __$ctx.ctx = __r1, "", __r2;
                return;
            } else {
                return $536(__$ctx);
            }
        } else {
            return $536(__$ctx);
        }
    }
    function $498(__$ctx) {
        var _$11ie = __$ctx.ctx.ie;
        if (_$11ie === true) {
            {
                "";
                var __r2 = __$ctx._mode;
                __$ctx._mode = "";
                var __r3 = __$ctx.ctx;
                __$ctx.ctx = [ 6, 7, 8, 9 ].map(function(v) {
                    return {
                        elem: "css",
                        url: this.ctx.url + ".ie" + v + ".css",
                        ie: "IE " + v
                    };
                }, __$ctx);
                $536(__$ctx);
                __$ctx._mode = __r2;
                __$ctx.ctx = __r3;
                "";
            }
            undefined;
        } else {
            var _$11hideRule = !_$11ie ? [ "gt IE 9", "<!-->", "<!--" ] : _$11ie === "!IE" ? [ _$11ie, "<!-->", "<!--" ] : [ _$11ie, "", "" ];
            {
                "";
                var __r4 = __$ctx._mode;
                __$ctx._mode = "";
                var __r5 = __$ctx.ctx, __r6 = __r5._ieCommented;
                __r5._ieCommented = true;
                var __r7 = __$ctx.ctx;
                __$ctx.ctx = [ "<!--[if " + _$11hideRule[0] + "]>", _$11hideRule[1], __$ctx.ctx, _$11hideRule[2], "<![endif]-->" ];
                $536(__$ctx);
                __$ctx._mode = __r4;
                __r5._ieCommented = __r6;
                __$ctx.ctx = __r7;
                "";
            }
            undefined;
        }
        return;
    }
    function $503(__$ctx) {
        if (!(__$ctx["__$anflg1"] !== true) === false) {
            if (!!__$ctx.elem === false) {
                return $506(__$ctx);
            } else {
                return $536(__$ctx);
            }
        } else {
            return $536(__$ctx);
        }
    }
    function $506(__$ctx) {
        var __r0, __r1, __r2, __r3;
        var _$lctx = __$ctx.ctx, _$ldtype = ("", __r0 = __$ctx._mode, __$ctx._mode = "doctype", __r1 = $529(__$ctx), __$ctx._mode = __r0, "", __r1), _$lxUA = ("", __r2 = __$ctx._mode, __$ctx._mode = "xUACompatible", __r3 = $521(__$ctx), __$ctx._mode = __r2, "", __r3), _$lbuf = [ _$ldtype, {
            elem: "root",
            content: [ {
                elem: "head",
                content: [ {
                    tag: "meta",
                    attrs: {
                        charset: "utf-8"
                    }
                }, _$lxUA, {
                    tag: "title",
                    content: _$lctx.title
                }, _$lctx.favicon ? {
                    elem: "favicon",
                    url: _$lctx.favicon
                } : "", _$lctx.meta, {
                    block: "i-ua"
                }, _$lctx.head ]
            }, _$lctx ]
        } ];
        {
            "";
            var __r4 = __$ctx["__$anflg1"];
            __$ctx["__$anflg1"] = true;
            {
                "";
                var __r5 = __$ctx.ctx;
                __$ctx.ctx = _$lbuf;
                var __r6 = __$ctx._mode;
                __$ctx._mode = "";
                $536(__$ctx);
                __$ctx.ctx = __r5;
                __$ctx._mode = __r6;
                "";
            }
            __$ctx["__$anflg1"] = __r4;
            "";
        }
        undefined;
        return;
    }
    function $521(__$ctx) {
        return __$ctx.ctx["x-ua-compatible"] === false ? false : {
            tag: "meta",
            attrs: {
                "http-equiv": "X-UA-Compatible",
                content: __$ctx.ctx["x-ua-compatible"] || "IE=edge"
            }
        };
        return;
    }
    function $529(__$ctx) {
        return __$ctx.ctx.doctype || "<!DOCTYPE html>";
        return;
    }
    function $535(__$ctx) {
        return undefined;
        return;
    }
    function $536(__$ctx) {
        if (!__$ctx.ctx === false) {
            if (!__$ctx.ctx.link === false) {
                if (!!__$ctx._.isSimple(__$ctx.ctx) === false) {
                    return $540(__$ctx);
                } else {
                    return $545(__$ctx);
                }
            } else {
                return $545(__$ctx);
            }
        } else {
            return $545(__$ctx);
        }
    }
    function $540(__$ctx) {
        var __r0, __r1;
        function _$6follow() {
            if (this.ctx.link === "no-follow") {
                return undefined;
            } else {
                undefined;
            }
            var data = this._links[this.ctx.link];
            return "", __r0 = this.ctx, this.ctx = data, __r1 = $1(__$ctx), this.ctx = __r0, "", __r1;
        }
        if (!cache || !__$ctx._cacheLog) {
            return _$6follow.call(__$ctx);
        } else {
            undefined;
        }
        var _$6contents = __$ctx._buf.slice(__$ctx._cachePos).join("");
        __$ctx._cachePos = __$ctx._buf.length;
        __$ctx._cacheLog.push(_$6contents, {
            log: __$ctx._localLog.slice(),
            link: __$ctx.ctx.link
        });
        var _$6res = _$6follow.call(__$ctx);
        __$ctx._cachePos = __$ctx._buf.length;
        return _$6res;
        return;
    }
    function $545(__$ctx) {
        if (!cache === false) {
            if (!__$ctx.ctx === false) {
                if (!__$ctx.ctx.cache === false) {
                    return $549(__$ctx);
                } else {
                    return $554(__$ctx);
                }
            } else {
                return $554(__$ctx);
            }
        } else {
            return $554(__$ctx);
        }
    }
    function $549(__$ctx) {
        var _$5cached;
        function _$5setProperty(obj, key, value) {
            if (key.length === 0) {
                return undefined;
            } else {
                undefined;
            }
            if (Array.isArray(value)) {
                var target = obj;
                for (var i = 0; i < value.length - 1; i++) {
                    target = target[value[i]];
                }
                value = target[value[i]];
            } else {
                undefined;
            }
            var host = obj, previous;
            for (var i = 0; i < key.length - 1; i++) {
                host = host[key[i]];
            }
            previous = host[key[i]];
            host[key[i]] = value;
            return previous;
        }
        if (_$5cached = cache.get(__$ctx.ctx.cache)) {
            var _$5oldLinks = __$ctx._links;
            if (__$ctx.ctx.links) {
                __$ctx._links = __$ctx.ctx.links;
            } else {
                undefined;
            }
            for (var _$5i = 0; _$5i < _$5cached.log.length; _$5i++) {
                if (typeof _$5cached.log[_$5i] === "string") {
                    __$ctx._buf.push(_$5cached.log[_$5i]);
                    continue;
                } else {
                    undefined;
                }
                var _$5log = _$5cached.log[_$5i], _$5reverseLog;
                _$5reverseLog = _$5log.log.map(function(entry) {
                    return {
                        key: entry[0],
                        value: _$5setProperty(this, entry[0], entry[1])
                    };
                }, __$ctx).reverse();
                {
                    "";
                    var __r0 = __$ctx.ctx, __r1 = __r0.cache;
                    __r0.cache = null;
                    var __r2 = __$ctx._cacheLog;
                    __$ctx._cacheLog = null;
                    var __r3 = __$ctx.ctx, __r4 = __r3.link;
                    __r3.link = _$5log.link;
                    $1(__$ctx);
                    __r0.cache = __r1;
                    __$ctx._cacheLog = __r2;
                    __r3.link = __r4;
                    "";
                }
                undefined;
                _$5reverseLog.forEach(function(entry) {
                    _$5setProperty(this, entry.key, entry.value);
                }, __$ctx);
            }
            __$ctx._links = _$5oldLinks;
            return _$5cached.res;
        } else {
            undefined;
        }
        var _$5cacheLog = [], _$5res;
        {
            "";
            var __r5 = __$ctx.ctx, __r6 = __r5.cache;
            __r5.cache = null;
            var __r7 = __$ctx._cachePos;
            __$ctx._cachePos = __$ctx._buf.length;
            var __r8 = __$ctx._cacheLog;
            __$ctx._cacheLog = _$5cacheLog;
            var __r9 = __$ctx._localLog;
            __$ctx._localLog = [];
            {
                _$5res = $1(__$ctx);
                var _$5tail = __$ctx._buf.slice(__$ctx._cachePos).join("");
                if (_$5tail) {
                    _$5cacheLog.push(_$5tail);
                } else {
                    undefined;
                }
            }
            __r5.cache = __r6;
            __$ctx._cachePos = __r7;
            __$ctx._cacheLog = __r8;
            __$ctx._localLog = __r9;
            "";
        }
        cache.set(__$ctx.ctx.cache, {
            log: _$5cacheLog,
            res: _$5res
        });
        return _$5res;
        return;
    }
    function $554(__$ctx) {
        var __t = __$ctx._mode;
        if (__t === "default") {
            return $556(__$ctx);
        } else if (__t === "") {
            if (!__$ctx._.isSimple(__$ctx.ctx) === false) {
                __$ctx._listLength--;
                var _$3ctx = __$ctx.ctx;
                (_$3ctx && _$3ctx !== true || _$3ctx === 0) && __$ctx._buf.push(_$3ctx);
                return;
            } else {
                if (!!__$ctx.ctx === false) {
                    __$ctx._listLength--;
                    return;
                } else {
                    if (!__$ctx._.isArray(__$ctx.ctx) === false) {
                        return $565(__$ctx);
                    } else {
                        if (!true === false) {
                            return $568(__$ctx);
                        } else {
                            return $e(__$ctx);
                        }
                    }
                }
            }
        } else {
            return $e(__$ctx);
        }
    }
    function $556(__$ctx) {
        var __r12, __r0, __r4, __r5, __r6, __r7, __r8, __r9, __r10, __r11, __r1, __r13, __r14, __r15, __r18, __r19, __r20, __r21, __r22, __r23;
        var _$4_this = __$ctx, _$4BEM_ = _$4_this.BEM, _$4v = __$ctx.ctx, _$4buf = __$ctx._buf, _$4tag;
        _$4tag = ("", __r0 = __$ctx._mode, __$ctx._mode = "tag", __r1 = $360(__$ctx), __$ctx._mode = __r0, "", __r1);
        typeof _$4tag != "undefined" || (_$4tag = _$4v.tag);
        typeof _$4tag != "undefined" || (_$4tag = "div");
        if (_$4tag) {
            var _$4jsParams, _$4js;
            if (__$ctx.block && _$4v.js !== false) {
                _$4js = ("", __r4 = __$ctx._mode, __$ctx._mode = "js", __r5 = $219(__$ctx), __$ctx._mode = __r4, "", __r5);
                _$4js = _$4js ? __$ctx._.extend(_$4v.js, _$4js === true ? {} : _$4js) : _$4v.js === true ? {} : _$4v.js;
                _$4js && ((_$4jsParams = {})[_$4BEM_.INTERNAL.buildClass(__$ctx.block, _$4v.elem)] = _$4js);
            } else {
                undefined;
            }
            _$4buf.push("<", _$4tag);
            var _$4isBEM = ("", __r6 = __$ctx._mode, __$ctx._mode = "bem", __r7 = $457(__$ctx), __$ctx._mode = __r6, "", __r7);
            typeof _$4isBEM != "undefined" || (_$4isBEM = typeof _$4v.bem != "undefined" ? _$4v.bem : _$4v.block || _$4v.elem);
            var _$4cls = ("", __r8 = __$ctx._mode, __$ctx._mode = "cls", __r9 = $181(__$ctx), __$ctx._mode = __r8, "", __r9);
            _$4cls || (_$4cls = _$4v.cls);
            var _$4addJSInitClass = _$4v.block && _$4jsParams;
            if (_$4isBEM || _$4cls) {
                _$4buf.push(' class="');
                if (_$4isBEM) {
                    _$4BEM_.INTERNAL.buildClasses(__$ctx.block, _$4v.elem, _$4v.elemMods || _$4v.mods, _$4buf);
                    var _$4mix = ("", __r10 = __$ctx._mode, __$ctx._mode = "mix", __r11 = $444(__$ctx), __$ctx._mode = __r10, "", __r11);
                    _$4v.mix && (_$4mix = _$4mix ? _$4mix.concat(_$4v.mix) : _$4v.mix);
                    if (_$4mix) {
                        var _$4visited = {};
                        function _$4visitedKey(block, elem) {
                            return (block || "") + "__" + (elem || "");
                        }
                        _$4visited[_$4visitedKey(__$ctx.block, __$ctx.elem)] = true;
                        if (!__$ctx._.isArray(_$4mix)) {
                            _$4mix = [ _$4mix ];
                        } else {
                            undefined;
                        }
                        for (var _$4i = 0; _$4i < _$4mix.length; _$4i++) {
                            var _$4mixItem = _$4mix[_$4i], _$4hasItem = _$4mixItem.block || _$4mixItem.elem, _$4block = _$4mixItem.block || _$4mixItem._block || _$4_this.block, _$4elem = _$4mixItem.elem || _$4mixItem._elem || _$4_this.elem;
                            _$4hasItem && _$4buf.push(" ");
                            _$4BEM_.INTERNAL[_$4hasItem ? "buildClasses" : "buildModsClasses"](_$4block, _$4mixItem.elem || _$4mixItem._elem || (_$4mixItem.block ? undefined : _$4_this.elem), _$4mixItem.elemMods || _$4mixItem.mods, _$4buf);
                            if (_$4mixItem.js) {
                                (_$4jsParams || (_$4jsParams = {}))[_$4BEM_.INTERNAL.buildClass(_$4block, _$4mixItem.elem)] = _$4mixItem.js === true ? {} : _$4mixItem.js;
                                _$4addJSInitClass || (_$4addJSInitClass = _$4block && !_$4mixItem.elem);
                            } else {
                                undefined;
                            }
                            if (_$4hasItem && !_$4visited[_$4visitedKey(_$4block, _$4elem)]) {
                                _$4visited[_$4visitedKey(_$4block, _$4elem)] = true;
                                var _$4nestedMix = ("", __r12 = __$ctx.block, __$ctx.block = _$4block, __r13 = __$ctx.elem, __$ctx.elem = _$4elem, __r14 = __$ctx._mode, __$ctx._mode = "mix", __r15 = $444(__$ctx), __$ctx.block = __r12, __$ctx.elem = __r13, __$ctx._mode = __r14, "", __r15);
                                if (_$4nestedMix) {
                                    for (var _$4j = 0; _$4j < _$4nestedMix.length; _$4j++) {
                                        var _$4nestedItem = _$4nestedMix[_$4j];
                                        if (!_$4nestedItem.block && !_$4nestedItem.elem || !_$4visited[_$4visitedKey(_$4nestedItem.block, _$4nestedItem.elem)]) {
                                            _$4nestedItem._block = _$4block;
                                            _$4nestedItem._elem = _$4elem;
                                            _$4mix.splice(_$4i + 1, 0, _$4nestedItem);
                                        } else {
                                            undefined;
                                        }
                                    }
                                } else {
                                    undefined;
                                }
                            } else {
                                undefined;
                            }
                        }
                    } else {
                        undefined;
                    }
                } else {
                    undefined;
                }
                _$4cls && _$4buf.push(_$4isBEM ? " " : "", _$4cls);
                _$4addJSInitClass && _$4buf.push(" i-bem");
                _$4buf.push('"');
            } else {
                undefined;
            }
            if (_$4jsParams) {
                var _$4jsAttr = ("", __r18 = __$ctx._mode, __$ctx._mode = "jsAttr", __r19 = $535(__$ctx), __$ctx._mode = __r18, "", __r19);
                _$4buf.push(" ", _$4jsAttr || "onclick", '="return ', __$ctx._.attrEscape(JSON.stringify(_$4jsParams)), '"');
            } else {
                undefined;
            }
            var _$4attrs = ("", __r20 = __$ctx._mode, __$ctx._mode = "attrs", __r21 = $307(__$ctx), __$ctx._mode = __r20, "", __r21);
            _$4attrs = __$ctx._.extend(_$4attrs, _$4v.attrs);
            if (_$4attrs) {
                var _$4name;
                for (_$4name in _$4attrs) {
                    if (_$4attrs[_$4name] === undefined) {
                        continue;
                    } else {
                        undefined;
                    }
                    _$4buf.push(" ", _$4name, '="', __$ctx._.attrEscape(_$4attrs[_$4name]), '"');
                }
            } else {
                undefined;
            }
        } else {
            undefined;
        }
        if (__$ctx._.isShortTag(_$4tag)) {
            _$4buf.push("/>");
        } else {
            _$4tag && _$4buf.push(">");
            var _$4content = ("", __r22 = __$ctx._mode, __$ctx._mode = "content", __r23 = $2(__$ctx), __$ctx._mode = __r22, "", __r23);
            if (_$4content || _$4content === 0) {
                var _$4isBEM = __$ctx.block || __$ctx.elem;
                {
                    "";
                    var __r24 = __$ctx._notNewList;
                    __$ctx._notNewList = false;
                    var __r25 = __$ctx.position;
                    __$ctx.position = _$4isBEM ? 1 : __$ctx.position;
                    var __r26 = __$ctx._listLength;
                    __$ctx._listLength = _$4isBEM ? 1 : __$ctx._listLength;
                    var __r27 = __$ctx.ctx;
                    __$ctx.ctx = _$4content;
                    var __r28 = __$ctx._mode;
                    __$ctx._mode = "";
                    $536(__$ctx);
                    __$ctx._notNewList = __r24;
                    __$ctx.position = __r25;
                    __$ctx._listLength = __r26;
                    __$ctx.ctx = __r27;
                    __$ctx._mode = __r28;
                    "";
                }
                undefined;
            } else {
                undefined;
            }
            _$4tag && _$4buf.push("</", _$4tag, ">");
        }
        return;
    }
    function $565(__$ctx) {
        var _$1v = __$ctx.ctx, _$1l = _$1v.length, _$1i = 0, _$1prevPos = __$ctx.position, _$1prevNotNewList = __$ctx._notNewList;
        if (_$1prevNotNewList) {
            __$ctx._listLength += _$1l - 1;
        } else {
            __$ctx.position = 0;
            __$ctx._listLength = _$1l;
        }
        __$ctx._notNewList = true;
        while (_$1i < _$1l) {
            var _$1newCtx = _$1v[_$1i++];
            {
                "";
                var __r0 = __$ctx.ctx;
                __$ctx.ctx = _$1newCtx === null ? "" : _$1newCtx;
                $536(__$ctx);
                __$ctx.ctx = __r0;
                "";
            }
            undefined;
        }
        _$1prevNotNewList || (__$ctx.position = _$1prevPos);
        return;
    }
    function $568(__$ctx) {
        var _$0vBlock = __$ctx.ctx.block, _$0vElem = __$ctx.ctx.elem, _$0block = __$ctx._currBlock || __$ctx.block;
        __$ctx.ctx || (__$ctx.ctx = {});
        {
            "";
            var __r0 = __$ctx._mode;
            __$ctx._mode = "default";
            var __r1 = __$ctx._links;
            __$ctx._links = __$ctx.ctx.links || __$ctx._links;
            var __r2 = __$ctx.block;
            __$ctx.block = _$0vBlock || (_$0vElem ? _$0block : undefined);
            var __r3 = __$ctx._currBlock;
            __$ctx._currBlock = _$0vBlock || _$0vElem ? undefined : _$0block;
            var __r4 = __$ctx.elem;
            __$ctx.elem = __$ctx.ctx.elem;
            var __r5 = __$ctx.mods;
            __$ctx.mods = (_$0vBlock ? __$ctx.ctx.mods : __$ctx.mods) || {};
            var __r6 = __$ctx.elemMods;
            __$ctx.elemMods = __$ctx.ctx.elemMods || {};
            {
                __$ctx.block || __$ctx.elem ? __$ctx.position = (__$ctx.position || 0) + 1 : __$ctx._listLength--;
                $493(__$ctx);
                undefined;
            }
            __$ctx._mode = __r0;
            __$ctx._links = __r1;
            __$ctx.block = __r2;
            __$ctx._currBlock = __r3;
            __$ctx.elem = __r4;
            __$ctx.mods = __r5;
            __$ctx.elemMods = __r6;
            "";
        }
        return;
    }
    function $e(__$ctx) {
        throw new Error(this);
        return;
    }
    !function() {
        var BEM_ = {}, toString = Object.prototype.toString, SHORT_TAGS = {
            area: 1,
            base: 1,
            br: 1,
            col: 1,
            command: 1,
            embed: 1,
            hr: 1,
            img: 1,
            input: 1,
            keygen: 1,
            link: 1,
            meta: 1,
            param: 1,
            source: 1,
            wbr: 1
        };
        (function(BEM, undefined) {
            var MOD_DELIM = "_", ELEM_DELIM = "__", NAME_PATTERN = "[a-zA-Z0-9-]+";
            function buildModPostfix(modName, modVal, buffer) {
                buffer.push(MOD_DELIM, modName, MOD_DELIM, modVal);
            }
            function buildBlockClass(name, modName, modVal, buffer) {
                buffer.push(name);
                modVal && buildModPostfix(modName, modVal, buffer);
            }
            function buildElemClass(block, name, modName, modVal, buffer) {
                buildBlockClass(block, undefined, undefined, buffer);
                buffer.push(ELEM_DELIM, name);
                modVal && buildModPostfix(modName, modVal, buffer);
            }
            BEM.INTERNAL = {
                NAME_PATTERN: NAME_PATTERN,
                MOD_DELIM: MOD_DELIM,
                ELEM_DELIM: ELEM_DELIM,
                buildModPostfix: function(modName, modVal, buffer) {
                    var res = buffer || [];
                    buildModPostfix(modName, modVal, res);
                    return buffer ? res : res.join("");
                },
                buildClass: function(block, elem, modName, modVal, buffer) {
                    var typeOf = typeof modName;
                    if (typeOf == "string") {
                        if (typeof modVal != "string") {
                            buffer = modVal;
                            modVal = modName;
                            modName = elem;
                            elem = undefined;
                        } else {
                            undefined;
                        }
                    } else {
                        if (typeOf != "undefined") {
                            buffer = modName;
                            modName = undefined;
                        } else {
                            if (elem && typeof elem != "string") {
                                buffer = elem;
                                elem = undefined;
                            } else {
                                undefined;
                            }
                        }
                    }
                    if (!(elem || modName || buffer)) {
                        return block;
                    } else {
                        undefined;
                    }
                    var res = buffer || [];
                    elem ? buildElemClass(block, elem, modName, modVal, res) : buildBlockClass(block, modName, modVal, res);
                    return buffer ? res : res.join("");
                },
                buildModsClasses: function(block, elem, mods, buffer) {
                    var res = buffer || [];
                    if (mods) {
                        var modName;
                        for (modName in mods) {
                            if (!mods.hasOwnProperty(modName)) {
                                continue;
                            } else {
                                undefined;
                            }
                            var modVal = mods[modName];
                            if (modVal == null) {
                                continue;
                            } else {
                                undefined;
                            }
                            modVal = mods[modName] + "";
                            if (!modVal) {
                                continue;
                            } else {
                                undefined;
                            }
                            res.push(" ");
                            if (elem) {
                                buildElemClass(block, elem, modName, modVal, res);
                            } else {
                                buildBlockClass(block, modName, modVal, res);
                            }
                        }
                    } else {
                        undefined;
                    }
                    return buffer ? res : res.join("");
                },
                buildClasses: function(block, elem, mods, buffer) {
                    var res = buffer || [];
                    elem ? buildElemClass(block, elem, undefined, undefined, res) : buildBlockClass(block, undefined, undefined, res);
                    this.buildModsClasses(block, elem, mods, buffer);
                    return buffer ? res : res.join("");
                }
            };
        })(BEM_);
        var buildEscape = function() {
            var ts = {
                '"': "&quot;",
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;"
            }, f = function(t) {
                return ts[t] || t;
            };
            return function(r) {
                r = new RegExp(r, "g");
                return function(s) {
                    return ("" + s).replace(r, f);
                };
            };
        }();
        function BEMContext(context, apply_) {
            this.ctx = typeof context === null ? "" : context;
            this.apply = apply_;
            this._buf = [];
            this._ = this;
            this._start = true;
            this._mode = "";
            this._listLength = 0;
            this._notNewList = false;
            this.position = 0;
            this.block = undefined;
            this.elem = undefined;
            this.mods = undefined;
            this.elemMods = undefined;
        }
        BEMContext.prototype.isArray = function isArray(obj) {
            return toString.call(obj) === "[object Array]";
        };
        BEMContext.prototype.isSimple = function isSimple(obj) {
            var t = typeof obj;
            return t === "string" || t === "number" || t === "boolean";
        };
        BEMContext.prototype.isShortTag = function isShortTag(t) {
            return SHORT_TAGS.hasOwnProperty(t);
        };
        BEMContext.prototype.extend = function extend(o1, o2) {
            if (!o1 || !o2) {
                return o1 || o2;
            } else {
                undefined;
            }
            var res = {}, n;
            for (n in o1) {
                o1.hasOwnProperty(n) && (res[n] = o1[n]);
            }
            for (n in o2) {
                o2.hasOwnProperty(n) && (res[n] = o2[n]);
            }
            return res;
        };
        BEMContext.prototype.identify = function() {
            var cnt = 0, id = BEM_["__id"] = +(new Date), expando = "__" + id, get = function() {
                return "uniq" + id + ++cnt;
            };
            return function(obj, onlyGet) {
                if (!obj) {
                    return get();
                } else {
                    undefined;
                }
                if (onlyGet || obj[expando]) {
                    return obj[expando];
                } else {
                    return obj[expando] = get();
                }
            };
        }();
        BEMContext.prototype.xmlEscape = buildEscape("[&<>]");
        BEMContext.prototype.attrEscape = buildEscape('["&<>]');
        BEMContext.prototype.BEM = BEM_;
        BEMContext.prototype.isFirst = function isFirst() {
            return this.position === 1;
        };
        BEMContext.prototype.isLast = function isLast() {
            return this.position === this._listLength;
        };
        BEMContext.prototype.generateId = function generateId() {
            return this.identify(this.ctx);
        };
        exports.apply = BEMContext.apply = function _apply() {
            var ctx = new BEMContext(this, apply);
            ctx.apply();
            return ctx._buf.join("");
        };
    }();
    return exports;
    exports.apply = apply;
    function apply(ctx) {
        return applyc(ctx || this);
    }
    function applyc(__$ctx) {
        return $1(__$ctx);
    }
    return exports;
})(typeof exports === "undefined" ? {} : exports);;
  return function(options) {
    var context = this;
    if (!options) options = {};
    cache = options.cache;
    return function() {
      if (context === this) context = undefined;
      return xjst.apply.call(
[context]
      );
    }.call(null);
  };
}();
typeof exports === "undefined" || (exports.BEMHTML = BEMHTML);