({
    block: 'b-page',
    'x-ua-compatible': 'IE=edge,chrome=1',
    title: 'Карточка товара',
    head: [
        { elem: 'js', url: '../../js/jquery-1.10.0.min.js'},
        { elem: 'css', url: 'product-default.css'},
        { elem: 'css', url: 'product-default.ie.css', ie: 'ie'}
    ],
    content: [
        {
            block: 'b-page-inner',
            content: [
                {
                    block: 'b-header'
                },
                {
                    block: 'b-content',
                    content: [
                        {
                            elem: 'row',
                            elemMods: {color: 'menu'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-great-menu',
                                    list: [
                                        {
                                            title: 'Каталог',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Автосигнализации',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Предпусковые подогреватели',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'hub'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-hub',
                                    list: [
                                        {
                                            url: '#',
                                            title: 'Автосигнализации'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Web'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        },
                                        {
                                            url: '#',
                                            title: 'Авто'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Webasto'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            content: {
                                block: 'b-container',
                                content: [
                                    {
                                        block: 'b-pathway',
                                        list: [
                                            {
                                                url: '#',
                                                title: 'Каталог товаров'
                                            },
                                            {
                                                url: '#',
                                                title: 'Противоугонные устройства'
                                            }
                                        ]
                                    },
                                    {
                                        block: 'b-card',
                                        title: 'Автосигнализация с автозапуском StarLine A91 Dialog',
                                        article: '3567213',
                                        price: '4950',
                                        images: [
                                            {
                                                preview: '../../images/temp/card-small1.jpg',
                                                image: '../../images/temp/card-middle1.jpg',
                                                full: '../../images/temp/card1.jpg'
                                            },
                                            {
                                                preview: '../../images/temp/card-small2.jpg',
                                                image: '../../images/temp/card-middle2.jpg',
                                                full: '../../images/temp/card2.jpg'
                                            },
                                            {
                                                preview: '../../images/temp/card-small3.jpg',
                                                image: '../../images/temp/card-middle3.jpg',
                                                full: '../../images/temp/card3.jpg'
                                            },
                                            {
                                                preview: '../../images/temp/card-small4.jpg',
                                                image: '../../images/temp/card-middle4.jpg',
                                                full: '../../images/temp/card4.jpg'
                                            },
                                            {
                                                preview: '../../images/temp/card-small5.jpg',
                                                image: '../../images/temp/card-middle5.jpg',
                                                full: '../../images/temp/card5.jpg'
                                            },
                                            {
                                                preview: '../../images/temp/card-small6.jpg',
                                                image: '../../images/temp/card-middle6.jpg',
                                                full: '../../images/temp/card6.jpg'
                                            }
                                        ],
                                        brand: {
                                            image: '../../images/temp/card-brand.jpg',
                                            url: '#'
                                        },
                                        discription: 'Надежная автомобильная охранная система с диалоговой авторизацией, индивидуальными ключами шифрования 128 бит и функцией интеллектуального автозапуска.  Рассчитана на работу в условиях экстремальных городских радиопомех. составе охранного комплекса StarLine «Победит».',
                                        features: [
                                            {
                                                title: 'Дальность действия',
                                                value: '1500 м'
                                            },
                                            {
                                                title: 'Обратная связь, запуск двигателя',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Брелок с ЖК-дисплеем, Турботаймер',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Виброоповещение, Anti-Hi-Jack',
                                                value: 'Есть'
                                            }
                                        ],
                                        related: [
                                            {
                                                url: '#',
                                                title: 'Автомобильная сирена Proma SC-532',
                                                image: '../../images/temp/related-product1.png',
                                                brand: 'Premier',
                                                price: '583'
                                            },
                                            {
                                                url: '#',
                                                title: 'GSM и GPS система охраны StarLine M21',
                                                image: '../../images/temp/related-product2.png',
                                                brand: 'StarLine',
                                                price: '4190'
                                            },
                                            {
                                                url: '#',
                                                title: 'Boomerang BPM модуль обхода штатного иммобилайзера',
                                                image: '../../images/temp/related-product3.png',
                                                brand: 'Boomerang',
                                                price: '350'
                                            },
                                            {
                                                url: '#',
                                                title: 'Автомобильная сирена Proma SC-532',
                                                image: '../../images/temp/related-product1.png',
                                                brand: 'Premier',
                                                price: '583'
                                            },
                                            {
                                                url: '#',
                                                title: 'GSM и GPS система охраны StarLine M21',
                                                image: '../../images/temp/related-product2.png',
                                                brand: 'StarLine',
                                                price: '4190'
                                            },
                                            {
                                                url: '#',
                                                title: 'Boomerang BPM модуль обхода штатного иммобилайзера',
                                                image: '../../images/temp/related-product3.png',
                                                brand: 'Boomerang',
                                                price: '350'
                                            },
                                            {
                                                url: '#',
                                                title: 'Автомобильная сирена Proma SC-532',
                                                image: '../../images/temp/related-product1.png',
                                                brand: 'Premier',
                                                price: '583'
                                            },
                                            {
                                                url: '#',
                                                title: 'GSM и GPS система охраны StarLine M21',
                                                image: '../../images/temp/related-product2.png',
                                                brand: 'StarLine',
                                                price: '4190'
                                            },
                                            {
                                                url: '#',
                                                title: 'Boomerang BPM модуль обхода штатного иммобилайзера',
                                                image: '../../images/temp/related-product3.png',
                                                brand: 'Boomerang',
                                                price: '350'
                                            }
                                        ],
                                        tech: [
                                            {
                                                title: 'Направленность',
                                                value: 'Двусторонняя'
                                            },
                                            {
                                                title: 'Индикация температуры в салоне авто',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Индикация разряда батареи',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Подсветка ЖКИ дисплея',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Режим Valet',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Брелок с ЖКИ дисплеем',
                                                value: '1'
                                            },
                                            {
                                                title: 'Сирена в комплекте',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Индикация времени',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Виброрежим на брелке-пейджере',
                                                value: 'Есть'
                                            },
                                            {
                                                title: 'Радиус действия брелока при приёме сигналов от системы',
                                                value: '1500 м'
                                            },
                                            {
                                                title: 'Радиус действия брелока при передаче команды',
                                                value: '1500 м'
                                            },
                                            {
                                                title: 'Брелок управления системой (классический)',
                                                value: '1'
                                            }
                                        ],
                                        text: {
                                            title: 'Описание автосигнализации с автозапуском StarLine A91 Dialog',
                                            list: [
                                                {
                                                    content: '<p>Диалоговая авторизация исключает интеллектуальный электронный взлом и обеспечивает устойчивость ко всем известным кодграбберам. Для защиты кода использован наиболее совершенный в настоящий момент алгоритм диалогового кодирования с индивидуальными ключами шифрования 128 бит,  а также инновационный метод прыгающих частот. При передаче команд трансивер многократно меняет частоты по специальной программе в периоде каждой посылки. Решение такого уровня, известное под техническим термином «метод расширения спектра со скачкообразной перестройкой частот», впервые в мире используется в системе управления сигнализацией и является весьма существенным усложнением каких-либо попыток взлома кода. Код шифрования «Диалог» используется как в основном, так и в дополнительном брелке.</p><p>Защищенность кода подтверждается долгосрочным контрактом СтарЛайн на 5 000 000 рублей для специалистов по электронному взлому.</p><p>Режим «Мегаполис». Увеличенная дальность управления и оповещения, а также уверенная работа в условиях экстремальных городских радиопомех обеспечивается использованием 128-канального узкополосного запатентованного OEM-трансивера с частотной модуляцией. Специализированная программа обработки сигналов, узкополосные фильтры, а также каналы приема и передачи, оптимальным образом распределенные по краям частотного диапазона 433.92 МГц, позволили нам на 8-10 Дб улучшить соотношение «сигнал-шум» и увеличить в 2 раза дальность управления и оповещения. Забудьте о радиопомехах на крупных парковках.</p>',
                                                    image: '../../images/temp/card-text1.png'
                                                },
                                                {
                                                    content: '<p>Диалоговая авторизация исключает интеллектуальный электронный взлом и обеспечивает устойчивость ко всем известным кодграбберам. Для защиты кода использован наиболее совершенный в настоящий момент алгоритм диалогового кодирования с индивидуальными ключами шифрования 128 бит,  а также инновационный метод прыгающих частот. При передаче команд трансивер многократно меняет частоты по специальной программе в периоде каждой посылки. Решение такого уровня, известное под техническим термином «метод расширения спектра со скачкообразной перестройкой частот», впервые в мире используется в системе управления сигнализацией и является весьма существенным усложнением каких-либо попыток взлома кода. Код шифрования «Диалог» используется как в основном, так и в дополнительном брелке.</p><p>Защищенность кода подтверждается долгосрочным контрактом СтарЛайн на 5 000 000 рублей для специалистов по электронному взлому.</p><p>Режим «Мегаполис». Увеличенная дальность управления и оповещения, а также уверенная работа в условиях экстремальных городских радиопомех обеспечивается использованием 128-канального узкополосного запатентованного OEM-трансивера с частотной модуляцией. Специализированная программа обработки сигналов, узкополосные фильтры, а также каналы приема и передачи, оптимальным образом распределенные по краям частотного диапазона 433.92 МГц, позволили нам на 8-10 Дб улучшить соотношение «сигнал-шум» и увеличить в 2 раза дальность управления и оповещения. Забудьте о радиопомехах на крупных парковках.</p>',
                                                    image: '../../images/temp/card-text2.png'
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'shadow'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-product-list',
                                    mods: {type: 'simile'},
                                    title: 'Похожие товары',
                                    content: [
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            mods: {type: 'new'},
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            mods: {type: 'sale'},
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        }
                                    ]
                                }
                            }
                        }
                    ]
                },
                {
                    elem: 'footer'
                }
            ]
        },
        {
            block: 'b-footer'
        },
        {
            block: 'b-scripts',
            content: [
                { elem: 'js', url:'../merged/_merged.js'}
            ]
        }
    ]
})
