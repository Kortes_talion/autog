({
    block: 'b-page',
    'x-ua-compatible': 'IE=edge,chrome=1',
    title: 'Корзина',
    head: [
        { elem: 'js', url: '../../js/jquery-1.10.0.min.js'},
        { elem: 'css', url: 'basket.css'},
        { elem: 'css', url: 'basket.ie.css', ie: 'ie'}
    ],
    content: [
        {
            block: 'b-page-inner',
            content: [
                {
                    block: 'b-header'
                },
                {
                    block: 'b-content',
                    content: [
                        {
                            elem: 'row',
                            elemMods: {color: 'menu'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-great-menu',
                                    list: [
                                        {
                                            title: 'Каталог',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Автосигнализации',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Предпусковые подогреватели',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'hub'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-hub',
                                    list: [
                                        {
                                            url: '#',
                                            title: 'Автосигнализации'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Web'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        },
                                        {
                                            url: '#',
                                            title: 'Авто'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Webasto'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            content: {
                                block: 'b-container',
                                content: [
                                    {
                                        block: 'b-pathway',
                                        title: 'Корзина'
                                    },
                                    {
                                        block: 'b-order',
                                        price: '5 300',
                                        content: [
                                            {
                                                block: 'b-order-item',
                                                image: '../../images/temp/basket-product.png',
                                                title: 'Автосигнализация StarLine A91',
                                                url: '#',
                                                brand: '../../images/temp/brand-basket.png',
                                                count: '1',
                                                price: '5 300'
                                            },
                                            {
                                                block: 'b-order-item',
                                                image: '../../images/temp/basket-product.png',
                                                title: 'Автосигнализация StarLine A91',
                                                url: '#',
                                                brand: '../../images/temp/brand-basket.png',
                                                count: '1',
                                                price: '5 300'
                                            }
                                        ]
                                    },
                                    {
                                        block: 'b-order-form'
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    elem: 'footer'
                }
            ]
        },
        {
            block: 'b-footer'
        },
        {
            block: 'b-scripts',
            content: [
                { elem: 'js', url:'../merged/_merged.bemhtml.js'},
                { elem: 'js', url:'../merged/_merged.js'}
            ]
        }
    ]
})
