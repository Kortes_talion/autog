({
    block: 'b-page',
    'x-ua-compatible': 'IE=edge,chrome=1',
    title: 'Главная страница',
    head: [
        { elem: 'js', url: '../../js/jquery-1.10.0.min.js'},
        { elem: 'css', url: 'index.css'},
        { elem: 'css', url: 'index.ie.css', ie: 'ie'}
    ],
    content: [
        {
            block: 'b-page-inner',
            content: [
                {
                    block: 'b-header'
                },
                {
                    block: 'b-content',
                    content: [
                        {
                            elem: 'row',
                            elemMods: {color: 'menu'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-great-menu',
                                    list: [
                                        {
                                            title: 'Каталог',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Автосигнализации',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            title: 'Предпусковые подогреватели',
                                            url: '#',
                                            list: [
                                                {
                                                    url: '#',
                                                    title: 'Противоугонные системы',
                                                    image: '../../images/temp/menu1.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Автосигнализации'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Брелоки для автосигнализаций'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Иммобилайзеры'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'GPS/GSM-модули'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Автозвук',
                                                    image: '../../images/temp/menu2.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'Сабвуферы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Динамики'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Провода'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Магнитолы'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Усилители'
                                                        }
                                                    ]
                                                },
                                                {
                                                    url: '#',
                                                    title: 'Предпусковые подогреватели двигателя',
                                                    image: '../../images/temp/menu3.png',
                                                    list: [
                                                        {
                                                            url: '#',
                                                            title: 'WEBASTO'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'HIDRONIC'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Пульты'
                                                        },
                                                        {
                                                            url: '#',
                                                            title: 'Аксессуары'
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'hub'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-hub',
                                    list: [
                                        {
                                            url: '#',
                                            title: 'Автосигнализации'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Web'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        },
                                        {
                                            url: '#',
                                            title: 'Авто'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Webasto'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-product-list',
                                    title: 'Новинки и акции',
                                    content: [
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            mods: {type: 'new'},
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            mods: {type: 'sale'},
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product1.png',
                                            price: '2800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация StarLine B9/A91',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product2.png',
                                            price: '2800',
                                            oldprice: '3800'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Scher-Khan Magicar 5',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product3.png',
                                            price: '6490'
                                        },
                                        {
                                            block: 'b-product',
                                            url: '#',
                                            title: 'Автосигнализация Tomahawk TZ-9030 с брелком',
                                            category: 'Автосигнализации',
                                            categorylink: '#',
                                            brand: '../../images/temp/brand.png',
                                            image: '../../images/temp/product4.png',
                                            price: '2990',
                                            oldprice: '3990'
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'shadow'},
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-info',
                                    list: [
                                        {
                                            title: 'Бесплатная доставка',
                                            text: 'При заказе на сумму <br />от 2 000 Р',
                                            image: '../../images/design/info1.png'
                                        },
                                        {
                                            title: 'Гарантия качества',
                                            text: 'Мы продаём только фирменную технику',
                                            image: '../../images/design/info2.png'
                                        },
                                        {
                                            title: 'Гибкая оплата',
                                            text: 'Оплата прямо с сайта любым удобным способом',
                                            image: '../../images/design/info3.png'
                                        }
                                    ]
                                }
                            }
                        },
                        {
                            elem: 'row',
                            elemMods: {color: 'grey'},
                            content: {
                                block: 'b-container',
                                cls: 'i-clearfix',
                                content: [
                                    {
                                        block: 'b-brand',
                                        url: '#',
                                        count: '57',
                                        list: [
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand1.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand2.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand3.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand4.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand5.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand6.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand7.png'
                                            },
                                            {
                                                url: '#',
                                                image: '../../images/temp/brand8.png'
                                            }
                                        ]
                                    },
                                    {
                                        block: 'b-seo',
                                        title: 'Интернет-магазин противоугонных систем',
                                        content: '<p>Автосигнализация — электронное устройство, установленное в автомобиль, предназначенное для его защиты от угона, кражи компонентов данного <a href="#">транспортного средства</a> или вещей, находящихся в автомобиле. Оно оповещает владельца о несанкционированном доступе к автомобилю (салону), но не препятствует угону, краже автокомпонентов и т.п.</p><p>Диалоговый код — специальный способ кодозащищённости автосигнализаций. Использует для идентификации брелка широко известную в криптографии технологию аутентификации через незащищённый канал. Получив сигнал, система убеждается, что он послан со «своего» брелка.</p>'
                                    }
                                ]
                            }
                        },
                        {
                            elem: 'row',
                            content: {
                                block: 'b-container',
                                content: {
                                    block: 'b-menu',
                                    mods: {type: 'horiz', position: 'hub'},
                                    list: [
                                        {
                                            url: '#',
                                            title: 'Автосигнализации'
                                        },
                                        {
                                            url: '#',
                                            title: 'Предпусковые подогреватели Webasto'
                                        },
                                        {
                                            url: '#',
                                            title: 'Коаксиальная автоакустика'
                                        },
                                        {
                                            url: '#',
                                            title: 'Потолочные мониторы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Брелоки для автосигнализаций'
                                        },
                                        {
                                            url: '#',
                                            title: 'Усилители'
                                        },
                                        {
                                            url: '#',
                                            title: 'Компонентная автоакустика'
                                        },
                                        {
                                            url: '#',
                                            title: 'Встраиваемые мониторы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Иммобилайзеры'
                                        },
                                        {
                                            url: '#',
                                            title: 'GPS/GSM-модули'
                                        },
                                        {
                                            url: '#',
                                            title: 'Штатная автоакустика'
                                        },
                                        {
                                            url: '#',
                                            title: 'Подголовники с монитором'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автомагнитолы'
                                        },
                                        {
                                            url: '#',
                                            title: 'Автосирены'
                                        },
                                        {
                                            url: '#',
                                            title: 'Компоненты автоакустики'
                                        },
                                        {
                                            url: '#',
                                            title: 'Салонные зеркала'
                                        }
                                    ]
                                }
                            }
                        }
                    ]
                },
                {
                    elem: 'footer'
                }
            ]
        },
        {
            block: 'b-footer'
        },
        {
            block: 'b-scripts',
            content: [
                { elem: 'js', url:'../merged/_merged.js'}
            ]
        }
    ]
})
