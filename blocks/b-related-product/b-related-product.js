(function(undefined) {
    BEM.DOM.decl('b-related-product', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.domElem.hover(function(){
                    $(this).find('.b-related-product__basket').animate({
                        width: 94
                    }, 500);
                },function(){
                    $(this).find('.b-related-product__basket').stop(true,true).css({
                        width: 18
                    });
                });
            }
        }
    });
})();
