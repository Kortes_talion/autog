({
    mustDeps: [
        {
            block: 'b-gallery'
        },
        {
            block: 'i-vendor',
            elem: 'carousel'
        },
        {
            block: 'i-vendor',
            elem: 'fancybox'
        },
        {
            block: 'b-related-product'
        }
    ],
    shouldDeps: []
})
