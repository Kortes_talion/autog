(function(undefined) {
    BEM.DOM.decl('b-card', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('list').customCarousel({
                    animation: 'slideTop',
                    displayed: 5,
                    infiniteLoop: false,
                    itemWidth: 72,
                    carouselHeight: 353,
                    nextItemSelector: bemThis.elem('arrow', 'position', 'next'),
                    prevItemSelector: bemThis.elem('arrow', 'position', 'prev'),
                    slideCallback: function(data){
                        if ((data.currentItem+1) >= data.itemsLength) {
                            bemThis.elem('arrow', 'position', 'next').addClass('b-card__arrow_state_disable');
                        } else {
                            bemThis.elem('arrow', 'position', 'next').removeClass('b-card__arrow_state_disable');
                        };
                        if (data.currentItem == 0) {
                            bemThis.elem('arrow', 'position', 'prev').addClass('b-card__arrow_state_disable');
                        } else {
                            bemThis.elem('arrow', 'position', 'prev').removeClass('b-card__arrow_state_disable');
                        }
                    }
                });



                bemThis.elem('related-all').click(function(e){
                    e.preventDefault();
                    bemThis.setMod(bemThis.elem('related-list'),'type', 'all');
                });
            }
        }
    });
})();
