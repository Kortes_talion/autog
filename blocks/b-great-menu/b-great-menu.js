(function(undefined) {
    BEM.DOM.decl('b-great-menu', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('link').hover(function(){
                    bemThis.elem('dropmenu').hide();
                    bemThis.delMod(bemThis.elem('item'), 'state');
                    $(this).closest('.b-great-menu__item').addClass('b-great-menu__item_state_hover');
                    $(this).closest('.b-great-menu__item').find('.b-great-menu__dropmenu').stop(true,true).slideDown(500);
                },function(){});

                bemThis.elem('item').hover(function(){},function(){
                    bemThis.elem('dropmenu').hide();
                    bemThis.delMod(bemThis.elem('item'), 'state');
                });
            }
        }
    });
})();
