(function(undefined) {
    BEM.DOM.decl('b-product-list', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;
                var widthAll = bemThis.domElem.width();
                var count = bemThis.findBlocksInside('b-product').length;
                var inner = bemThis.elem('inner').height();

                if (count < 4) {
                    bemThis.elem('toggle-text','position','min').text(count);
                } else if(widthAll < 1180 || count==4){
                    bemThis.elem('toggle-text','position','min').text('4');
                } else if(count >= 5){
                    bemThis.elem('toggle-text','position','min').text('5');
                }

                bemThis.elem('toggle-text','position','max').text(count);

                bemThis.elem('toggle').toggles({
                    checkbox:$('.checkme'),
                    'clicker': $('.clickme'),
                    'text': {
                      'on': ' ',
                      'off': ' '
                    },
                    height: 18,
                    width: 36
                });

                bemThis.findBlockInside('checkme').domElem.change(function(){
                    if ($(this).is(':checked')) {
                        var index = 1;
                        bemThis.elem('body').animate({
                            height: inner
                        }, 500);
                    } else {
                        var index = 0;
                        bemThis.elem('body').animate({
                            height: 320
                        }, 500);
                    };
                    bemThis.delMod(bemThis.elem('toggle-text'), 'state');
                    bemThis.setMod(bemThis.elem('toggle-text').eq(index), 'state', 'active');
                    bemThis.delMod(bemThis.elem('list'), 'state');
                    bemThis.setMod(bemThis.elem('list').eq(index), 'state', 'active');
                });
            }
        }
    });
})();
