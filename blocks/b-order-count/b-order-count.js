(function(undefined) {
    BEM.DOM.decl('b-order-count', {
        getStep: function() {
            return this.params.step || 1;
        },

        getValue: function() {
            var input = this.elem('input');
            var value = input.val();
            return parseInt(value, 10);
        },

        setValue: function(value) {
            var input = this.elem('input');
            value = this.filter(value);

            if (input.val() != value) {
                input.val(value).change();
                input.trigger('order-change', {
                    step: this.getStep(),
                    min: this.params.min,
                    max: this.params.max,
                    value: value
                });
            }
        },

        next: function(step) {
            step = step || this.getStep();
            this.setValue(this.getValue() + step);
        },

        prev: function(step) {
            if (!step) {
                step = this.getStep();
            }
            this.setValue(this.getValue() - step);
        },

        filter: function(value) {
            value = parseInt(value, 10) || 0;
            this.delMod(this.elem('link'), 'state');
            // Проверяем соответствие значений
            if (value <= this.params.min) {
                value = this.params.min;

                this.setMod(this.elem('link', 'type', 'less'), 'state', 'disable');
            }
            if (this.params.max <= value) {
                value = this.params.max;

                this.setMod(this.elem('link', 'type', 'more'), 'state', 'disable');
            }
            return value;
        },

        onSetMod : {
            'js' : function() {
                var bemThis = this;

                var input = this.elem('input');

                input.keypress(function(e) {
                    var chr = String.fromCharCode(e.which);
                    return "1234567890".indexOf(chr) >= 0;
                });

                input.keydown(function(e) {
                    if (e.keyCode == 38) {
                        bemThis.next();
                    } else if (e.keyCode == 40) {
                        bemThis.prev();
                    }
                });

                input.bind('paste', function() {
                    setTimeout(function () {
                        bemThis.setValue(input.val());
                    }, 300);
                });

                input.bind('change', function() {
                    setTimeout(function () {
                        bemThis.setValue(input.val());
                    }, 300);
                });

                this.elem('link', 'type', 'less').click(function(e) {
                    e.preventDefault();
                    bemThis.prev();
                });

                this.elem('link', 'type', 'more').click(function(e) {
                    e.preventDefault();
                    bemThis.next();
                });
            }
        }
    });
})();
