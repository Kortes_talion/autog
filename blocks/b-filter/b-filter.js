(function(undefined) {
    BEM.DOM.decl('b-filter', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.domElem.find('.b-form-control__input:checkbox').change(function(){
                    if ($(this).prop('checked')) {
                        $(this).closest('.b-filter__checkbox').addClass('checked');
                    } else {
                        $(this).closest('.b-filter__checkbox').removeClass('checked');
                    }
                });

                bemThis.domElem.find('.b-form-control__input:radio').change(function(){
                    if ($(this).prop('checked')) {
                        var name = $(this).attr('name');
                        $('b-.form-control__input:radio[name='+name+']').removeAttr('cheched').closest('.b-filter__radio').removeClass('checked');
                        $(this).closest('.b-form-control__label').addClass('checked');
                    }
                });

                $('.b-filter__item').click(function() {
                    var $this = $(this).closest('.b-filter__item');
                    if ($this.is('.b-filter__item_type_switcher-on')) {
                        $this
                           .removeClass('b-filter__item_type_switcher-on')
                           .addClass('b-filter__item_type_switcher-off')
                           .find('.b-filter__body')
                           .hide();
                    } else {
                        $this
                           .removeClass('b-filter__item_type_switcher-off')
                           .addClass('b-filter__item_type_switcher-on')
                           .find('.b-filter__body')
                           .slideDown(1000);
                    }
                });
            }
        }
    });
})();
