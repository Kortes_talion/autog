({
    mustDeps: [
        {
            block: 'b-form'
        },
        {
            block: 'b-form-control'
        },
        {
            block: 'b-form-control',
            mods: {type: ['checkbox']}
        },
        {
            block: 'b-range'
        }
    ],
    shouldDeps: []
})
