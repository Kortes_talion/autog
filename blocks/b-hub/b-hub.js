(function(undefined) {
    BEM.DOM.decl('b-hub', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                var wrapper = bemThis.elem('wrapper').height();

                var list = bemThis.elem('list').height();

                if (list > wrapper){
                    bemThis.setMod('state', 'all');
                }

                bemThis.elem('all').click(function(e){
                    e.preventDefault();
                    bemThis.delMod('state');
                    bemThis.elem('wrapper').animate({
                        height: list
                    }, 500);
                });
            }
        }
    });
})();
