(function(undefined) {
    BEM.DOM.decl('b-products', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('toggle').toggles({
                    checkbox:$('.checkme'),
                    'clicker': $('.clickme'),
                    'text': {
                      'on': ' ',
                      'off': ' '
                    },
                    height: 18,
                    width: 36
                });

                bemThis.findBlockInside('checkme').domElem.change(function(){
                    if ($(this).is(':checked')) {
                        var index = 1;
                    } else {
                        var index = 0;
                    };
                    bemThis.delMod(bemThis.elem('toggle-text'), 'state');
                    bemThis.setMod(bemThis.elem('toggle-text').eq(index), 'state', 'active');
                });
            }
        }
    });
})();
