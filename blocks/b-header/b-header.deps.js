({
    mustDeps: [
        {
            block: 'b-container'
        },
        {
            block: 'i-clearfix'
        },
        {
            block: 'b-link'
        },
        {
            block: 'b-icon'
        },
        {
            block: 'b-menu'
        },
        {
            block: 'b-search-form'
        },
        {
            block: 'b-basket'
        },
        {
            block: 'b-feedback'
        },
        {
            block: 'i-inline'
        },
        {
            block: 'i-font',
            mods: {face: ['opensans', 'opensans-semibold']}
        }
    ],
    shouldDeps: []
})
