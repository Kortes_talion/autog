(function(undefined) {
    BEM.DOM.decl('b-header', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;
                bemThis.elem('wrapper').each(function(){
                    var panel     = $(this);
                    var position  = panel.offset();

                    $(window).scroll(function () {
                        if ($(this).scrollTop() > position.top) {
                            bemThis.elem('scroll-block').css({
                                "position":"fixed",
                                "z-index":"1000"
                            });
                            bemThis.findBlockInside('b-menu').domElem.stop(true, true).slideUp(500);
                            bemThis.elem('search-info').stop(true, true).slideUp(500);
                            bemThis.elem('feedback').stop(true, true).slideUp(500);
                            bemThis.elem('phone').stop(true, true).slideUp(500);
                            bemThis.elem('logo').stop(true, true).animate({
                                "margin-top": 0
                            },500);
                        }
                        else {
                            bemThis.elem('scroll-block').css({
                                "position":"static"
                            });
                            bemThis.findBlockInside('b-menu').domElem.slideDown(150);
                            bemThis.elem('logo').animate({
                                "margin-top": 30
                            },500);
                            bemThis.elem('search-info').slideDown(150);
                            bemThis.elem('feedback').slideDown(150);
                            bemThis.elem('phone').slideDown(150);
                        }
                    });
                });
            }
        }
    });
})();
