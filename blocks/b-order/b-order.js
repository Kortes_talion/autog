(function(undefined) {
    BEM.DOM.decl('b-order', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                bemThis.elem('next').click(function(e){
                    e.preventDefault();
                    $(this).closest('.b-content').find('.b-order-form').slideDown(1000);
                });
            }
        }
    });
})();
