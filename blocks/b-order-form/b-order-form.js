(function(undefined) {
    BEM.DOM.decl('b-order-form', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;

                $.datepicker.regional['ru'] = {
                    closeText: 'Закрыть',
                    prevText: '&#x3c;Пред',
                    nextText: 'След&#x3e;',
                    currentText: 'Сегодня',
                    monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь', 'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                    monthNamesGenitive: ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"],
                    monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн', 'Июл','Авг','Сен','Окт','Ноя','Дек'],
                    dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
                    dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
                    dayNamesMin: ['вс','пн','вт','ср','чт','пт','сб'],
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1,
                    isRTL: false
                };
                $.datepicker.setDefaults($.datepicker.regional['ru']);

                bemThis.elem('phone').find('.b-form-control__input').mask('8(999) 999-99-99');

                bemThis.elem('date').find('.b-form-control__input').datepicker();
            }
        }
    });
})();
