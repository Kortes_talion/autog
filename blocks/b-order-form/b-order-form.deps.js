({
    mustDeps: [
        {
            block: 'b-form'
        },
        {
            block: 'b-form-control'
        },
        {
            block: 'b-form-control',
            mods: {type: ['text', 'textarea', 'radio']}
        },
        {
            block: 'b-debug-toolbar'
        },
        {
            block: 'i-vendor',
            elem: 'mask'
        },
        {
            block: 'i-vendor',
            elem: 'ui'
        }
    ]
})
