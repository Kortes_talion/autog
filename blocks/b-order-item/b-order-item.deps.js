({
    mustDeps: [
        {
            block: 'b-order-count'
        },
        {
            block: 'i-animated'
        },
        {
            block: 'i-animated',
            mods: {
                'type': 'fadeOutRight'
            }
        }
    ]
})
