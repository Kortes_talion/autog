/** @requires BEM */
/** @requires BEM.DOM */

(function(undefined) {
    BEM.DOM.decl('b-order-item', {
        onSetMod : {
            'js' : function() {
                var bemThis = this;
                this.elem('del').click(function() {
                    bemThis.domElem.trigger('b-order-item:remove');

                    bemThis.domElem
                        .addClass('i-animated i-animated_type_fadeOutRight')
                        .bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
                            bemThis.domElem.remove();
                        })
                        .bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function() {
                            bemThis.domElem.slideUp('fast', function() {
                                bemThis.domElem.remove();
                            });
                        });
                });
            }
        }
    });
})();
